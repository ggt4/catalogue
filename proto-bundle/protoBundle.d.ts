import * as $protobuf from 'protobufjs';
/** Namespace Catalogue. */
export namespace Catalogue {
  /** Represents a Greeter */
  class Greeter extends $protobuf.rpc.Service {
    /**
     * Constructs a new Greeter service.
     * @param rpcImpl RPC implementation
     * @param [requestDelimited=false] Whether requests are length-delimited
     * @param [responseDelimited=false] Whether responses are length-delimited
     */
    constructor(
      rpcImpl: $protobuf.RPCImpl,
      requestDelimited?: boolean,
      responseDelimited?: boolean,
    );

    /**
     * Creates new Greeter service using the specified rpc implementation.
     * @param rpcImpl RPC implementation
     * @param [requestDelimited=false] Whether requests are length-delimited
     * @param [responseDelimited=false] Whether responses are length-delimited
     * @returns RPC service. Useful where requests and/or responses are streamed.
     */
    public static create(
      rpcImpl: $protobuf.RPCImpl,
      requestDelimited?: boolean,
      responseDelimited?: boolean,
    ): Greeter;

    /**
     * Calls products.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseGetAll
     */
    public products(
      request: Product.IRequest,
      callback: Catalogue.Greeter.productsCallback,
    ): void;

    /**
     * Calls products.
     * @param request Request message or plain object
     * @returns Promise
     */
    public products(request: Product.IRequest): Promise<Product.ResponseGetAll>;

    /**
     * Calls product.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseDetail
     */
    public product(
      request: Product.IRequest,
      callback: Catalogue.Greeter.productCallback,
    ): void;

    /**
     * Calls product.
     * @param request Request message or plain object
     * @returns Promise
     */
    public product(request: Product.IRequest): Promise<Product.ResponseDetail>;

    /**
     * Calls productCreate.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseCreate
     */
    public productCreate(
      request: Product.IRequest,
      callback: Catalogue.Greeter.productCreateCallback,
    ): void;

    /**
     * Calls productCreate.
     * @param request Request message or plain object
     * @returns Promise
     */
    public productCreate(
      request: Product.IRequest,
    ): Promise<Product.ResponseCreate>;

    /**
     * Calls productUpdate.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseUpdate
     */
    public productUpdate(
      request: Product.IRequest,
      callback: Catalogue.Greeter.productUpdateCallback,
    ): void;

    /**
     * Calls productUpdate.
     * @param request Request message or plain object
     * @returns Promise
     */
    public productUpdate(
      request: Product.IRequest,
    ): Promise<Product.ResponseUpdate>;

    /**
     * Calls productDelete.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseDelete
     */
    public productDelete(
      request: Product.IRequest,
      callback: Catalogue.Greeter.productDeleteCallback,
    ): void;

    /**
     * Calls productDelete.
     * @param request Request message or plain object
     * @returns Promise
     */
    public productDelete(
      request: Product.IRequest,
    ): Promise<Product.ResponseDelete>;
  }

  namespace Greeter {
    /**
     * Callback as used by {@link Catalogue.Greeter#products}.
     * @param error Error, if any
     * @param [response] ResponseGetAll
     */
    type productsCallback = (
      error: Error | null,
      response?: Product.ResponseGetAll,
    ) => void;

    /**
     * Callback as used by {@link Catalogue.Greeter#product}.
     * @param error Error, if any
     * @param [response] ResponseDetail
     */
    type productCallback = (
      error: Error | null,
      response?: Product.ResponseDetail,
    ) => void;

    /**
     * Callback as used by {@link Catalogue.Greeter#productCreate}.
     * @param error Error, if any
     * @param [response] ResponseCreate
     */
    type productCreateCallback = (
      error: Error | null,
      response?: Product.ResponseCreate,
    ) => void;

    /**
     * Callback as used by {@link Catalogue.Greeter#productUpdate}.
     * @param error Error, if any
     * @param [response] ResponseUpdate
     */
    type productUpdateCallback = (
      error: Error | null,
      response?: Product.ResponseUpdate,
    ) => void;

    /**
     * Callback as used by {@link Catalogue.Greeter#productDelete}.
     * @param error Error, if any
     * @param [response] ResponseDelete
     */
    type productDeleteCallback = (
      error: Error | null,
      response?: Product.ResponseDelete,
    ) => void;
  }
}

/** Namespace Product. */
export namespace Product {
  /** Properties of a Request. */
  interface IRequest {
    /** Request authorization */
    authorization?: string | null;

    /** Request query */
    query?: Product.Request.IQuery | null;

    /** Request body */
    body?: Product.Request.IBody | null;

    /** Request params */
    params?: Product.Request.IParams | null;

    /** Request user */
    user?: Product.Request.IUser | null;
  }

  /** Represents a Request. */
  class Request implements IRequest {
    /**
     * Constructs a new Request.
     * @param [properties] Properties to set
     */
    constructor(properties?: Product.IRequest);

    /** Request authorization. */
    public authorization?: string | null;

    /** Request query. */
    public query?: Product.Request.IQuery | null;

    /** Request body. */
    public body?: Product.Request.IBody | null;

    /** Request params. */
    public params?: Product.Request.IParams | null;

    /** Request user. */
    public user?: Product.Request.IUser | null;

    /** Request _authorization. */
    public _authorization?: 'authorization';

    /** Request _query. */
    public _query?: 'query';

    /** Request _body. */
    public _body?: 'body';

    /** Request _params. */
    public _params?: 'params';

    /** Request _user. */
    public _user?: 'user';

    /**
     * Creates a new Request instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Request instance
     */
    public static create(properties?: Product.IRequest): Product.Request;

    /**
     * Encodes the specified Request message. Does not implicitly {@link Product.Request.verify|verify} messages.
     * @param message Request message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Product.IRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified Request message, length delimited. Does not implicitly {@link Product.Request.verify|verify} messages.
     * @param message Request message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Product.IRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a Request message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Product.Request;

    /**
     * Decodes a Request message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Product.Request;

    /**
     * Verifies a Request message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a Request message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Request
     */
    public static fromObject(object: { [k: string]: any }): Product.Request;

    /**
     * Creates a plain object from a Request message. Also converts values to other types if specified.
     * @param message Request
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Product.Request,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this Request to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace Request {
    /** Properties of a Query. */
    interface IQuery {
      /** Query pagination */
      pagination?: number | null;

      /** Query limit */
      limit?: number | null;

      /** Query filterBy */
      filterBy?: string | null;

      /** Query search */
      search?: string | null;

      /** Query lowerThan */
      lowerThan?: string | null;

      /** Query greaterThan */
      greaterThan?: string | null;

      /** Query ids */
      ids?: number[] | null;
    }

    /** Represents a Query. */
    class Query implements IQuery {
      /**
       * Constructs a new Query.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.Request.IQuery);

      /** Query pagination. */
      public pagination?: number | null;

      /** Query limit. */
      public limit?: number | null;

      /** Query filterBy. */
      public filterBy?: string | null;

      /** Query search. */
      public search?: string | null;

      /** Query lowerThan. */
      public lowerThan?: string | null;

      /** Query greaterThan. */
      public greaterThan?: string | null;

      /** Query ids. */
      public ids: number[];

      /** Query _pagination. */
      public _pagination?: 'pagination';

      /** Query _limit. */
      public _limit?: 'limit';

      /** Query _filterBy. */
      public _filterBy?: 'filterBy';

      /** Query _search. */
      public _search?: 'search';

      /** Query _lowerThan. */
      public _lowerThan?: 'lowerThan';

      /** Query _greaterThan. */
      public _greaterThan?: 'greaterThan';

      /**
       * Creates a new Query instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Query instance
       */
      public static create(
        properties?: Product.Request.IQuery,
      ): Product.Request.Query;

      /**
       * Encodes the specified Query message. Does not implicitly {@link Product.Request.Query.verify|verify} messages.
       * @param message Query message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.Request.IQuery,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Query message, length delimited. Does not implicitly {@link Product.Request.Query.verify|verify} messages.
       * @param message Query message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.Request.IQuery,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Query message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.Request.Query;

      /**
       * Decodes a Query message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.Request.Query;

      /**
       * Verifies a Query message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Query message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Query
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.Request.Query;

      /**
       * Creates a plain object from a Query message. Also converts values to other types if specified.
       * @param message Query
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.Request.Query,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Query to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Body. */
    interface IBody {
      /** Body productName */
      productName?: string | null;

      /** Body productLogo */
      productLogo?: string | null;

      /** Body productDescription */
      productDescription?: string | null;

      /** Body productShortDescription */
      productShortDescription?: string | null;

      /** Body metaTitleProduct */
      metaTitleProduct?: string | null;

      /** Body metaDescriptionProduct */
      metaDescriptionProduct?: string | null;

      /** Body productSlug */
      productSlug?: string | null;

      /** Body metaKeyword */
      metaKeyword?: string | null;

      /** Body price */
      price?: number | null;

      /** Body createdBy */
      createdBy?: number | null;

      /** Body productImage */
      productImage?: Product.Request.Body.IProductimage[] | null;

      /** Body productCategory */
      productCategory?: Product.Request.Body.IProductcategory[] | null;
    }

    /** Represents a Body. */
    class Body implements IBody {
      /**
       * Constructs a new Body.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.Request.IBody);

      /** Body productName. */
      public productName: string;

      /** Body productLogo. */
      public productLogo: string;

      /** Body productDescription. */
      public productDescription: string;

      /** Body productShortDescription. */
      public productShortDescription: string;

      /** Body metaTitleProduct. */
      public metaTitleProduct: string;

      /** Body metaDescriptionProduct. */
      public metaDescriptionProduct: string;

      /** Body productSlug. */
      public productSlug: string;

      /** Body metaKeyword. */
      public metaKeyword: string;

      /** Body price. */
      public price: number;

      /** Body createdBy. */
      public createdBy: number;

      /** Body productImage. */
      public productImage: Product.Request.Body.IProductimage[];

      /** Body productCategory. */
      public productCategory: Product.Request.Body.IProductcategory[];

      /**
       * Creates a new Body instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Body instance
       */
      public static create(
        properties?: Product.Request.IBody,
      ): Product.Request.Body;

      /**
       * Encodes the specified Body message. Does not implicitly {@link Product.Request.Body.verify|verify} messages.
       * @param message Body message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.Request.IBody,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Body message, length delimited. Does not implicitly {@link Product.Request.Body.verify|verify} messages.
       * @param message Body message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.Request.IBody,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Body message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.Request.Body;

      /**
       * Decodes a Body message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.Request.Body;

      /**
       * Verifies a Body message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Body message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Body
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.Request.Body;

      /**
       * Creates a plain object from a Body message. Also converts values to other types if specified.
       * @param message Body
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.Request.Body,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Body to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    namespace Body {
      /** Properties of a Productimage. */
      interface IProductimage {
        /** Productimage id */
        id?: number | null;

        /** Productimage productId */
        productId?: number | null;

        /** Productimage nameImage */
        nameImage?: string | null;

        /** Productimage altImage */
        altImage?: string | null;

        /** Productimage createdDate */
        createdDate?: string | null;

        /** Productimage updatedDate */
        updatedDate?: string | null;
      }

      /** Represents a Productimage. */
      class Productimage implements IProductimage {
        /**
         * Constructs a new Productimage.
         * @param [properties] Properties to set
         */
        constructor(properties?: Product.Request.Body.IProductimage);

        /** Productimage id. */
        public id: number;

        /** Productimage productId. */
        public productId: number;

        /** Productimage nameImage. */
        public nameImage: string;

        /** Productimage altImage. */
        public altImage: string;

        /** Productimage createdDate. */
        public createdDate: string;

        /** Productimage updatedDate. */
        public updatedDate: string;

        /**
         * Creates a new Productimage instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Productimage instance
         */
        public static create(
          properties?: Product.Request.Body.IProductimage,
        ): Product.Request.Body.Productimage;

        /**
         * Encodes the specified Productimage message. Does not implicitly {@link Product.Request.Body.Productimage.verify|verify} messages.
         * @param message Productimage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(
          message: Product.Request.Body.IProductimage,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Encodes the specified Productimage message, length delimited. Does not implicitly {@link Product.Request.Body.Productimage.verify|verify} messages.
         * @param message Productimage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(
          message: Product.Request.Body.IProductimage,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Decodes a Productimage message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(
          reader: $protobuf.Reader | Uint8Array,
          length?: number,
        ): Product.Request.Body.Productimage;

        /**
         * Decodes a Productimage message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(
          reader: $protobuf.Reader | Uint8Array,
        ): Product.Request.Body.Productimage;

        /**
         * Verifies a Productimage message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): string | null;

        /**
         * Creates a Productimage message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Productimage
         */
        public static fromObject(object: {
          [k: string]: any;
        }): Product.Request.Body.Productimage;

        /**
         * Creates a plain object from a Productimage message. Also converts values to other types if specified.
         * @param message Productimage
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(
          message: Product.Request.Body.Productimage,
          options?: $protobuf.IConversionOptions,
        ): { [k: string]: any };

        /**
         * Converts this Productimage to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
      }

      /** Properties of a Productcategory. */
      interface IProductcategory {
        /** Productcategory categoryName */
        categoryName?: string | null;

        /** Productcategory categorySlug */
        categorySlug?: string | null;
      }

      /** Represents a Productcategory. */
      class Productcategory implements IProductcategory {
        /**
         * Constructs a new Productcategory.
         * @param [properties] Properties to set
         */
        constructor(properties?: Product.Request.Body.IProductcategory);

        /** Productcategory categoryName. */
        public categoryName: string;

        /** Productcategory categorySlug. */
        public categorySlug: string;

        /**
         * Creates a new Productcategory instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Productcategory instance
         */
        public static create(
          properties?: Product.Request.Body.IProductcategory,
        ): Product.Request.Body.Productcategory;

        /**
         * Encodes the specified Productcategory message. Does not implicitly {@link Product.Request.Body.Productcategory.verify|verify} messages.
         * @param message Productcategory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(
          message: Product.Request.Body.IProductcategory,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Encodes the specified Productcategory message, length delimited. Does not implicitly {@link Product.Request.Body.Productcategory.verify|verify} messages.
         * @param message Productcategory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(
          message: Product.Request.Body.IProductcategory,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Decodes a Productcategory message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(
          reader: $protobuf.Reader | Uint8Array,
          length?: number,
        ): Product.Request.Body.Productcategory;

        /**
         * Decodes a Productcategory message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(
          reader: $protobuf.Reader | Uint8Array,
        ): Product.Request.Body.Productcategory;

        /**
         * Verifies a Productcategory message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): string | null;

        /**
         * Creates a Productcategory message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Productcategory
         */
        public static fromObject(object: {
          [k: string]: any;
        }): Product.Request.Body.Productcategory;

        /**
         * Creates a plain object from a Productcategory message. Also converts values to other types if specified.
         * @param message Productcategory
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(
          message: Product.Request.Body.Productcategory,
          options?: $protobuf.IConversionOptions,
        ): { [k: string]: any };

        /**
         * Converts this Productcategory to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
      }
    }

    /** Properties of a Params. */
    interface IParams {
      /** Params id */
      id?: string | null;

      /** Params ids */
      ids?: number[] | null;

      /** Params flag */
      flag?: string | null;
    }

    /** Represents a Params. */
    class Params implements IParams {
      /**
       * Constructs a new Params.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.Request.IParams);

      /** Params id. */
      public id: string;

      /** Params ids. */
      public ids: number[];

      /** Params flag. */
      public flag?: string | null;

      /** Params _flag. */
      public _flag?: 'flag';

      /**
       * Creates a new Params instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Params instance
       */
      public static create(
        properties?: Product.Request.IParams,
      ): Product.Request.Params;

      /**
       * Encodes the specified Params message. Does not implicitly {@link Product.Request.Params.verify|verify} messages.
       * @param message Params message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.Request.IParams,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Params message, length delimited. Does not implicitly {@link Product.Request.Params.verify|verify} messages.
       * @param message Params message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.Request.IParams,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Params message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.Request.Params;

      /**
       * Decodes a Params message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.Request.Params;

      /**
       * Verifies a Params message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Params message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Params
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.Request.Params;

      /**
       * Creates a plain object from a Params message. Also converts values to other types if specified.
       * @param message Params
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.Request.Params,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Params to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a User. */
    interface IUser {
      /** User id */
      id?: number | null;
    }

    /** Represents a User. */
    class User implements IUser {
      /**
       * Constructs a new User.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.Request.IUser);

      /** User id. */
      public id: number;

      /**
       * Creates a new User instance using the specified properties.
       * @param [properties] Properties to set
       * @returns User instance
       */
      public static create(
        properties?: Product.Request.IUser,
      ): Product.Request.User;

      /**
       * Encodes the specified User message. Does not implicitly {@link Product.Request.User.verify|verify} messages.
       * @param message User message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.Request.IUser,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified User message, length delimited. Does not implicitly {@link Product.Request.User.verify|verify} messages.
       * @param message User message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.Request.IUser,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a User message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.Request.User;

      /**
       * Decodes a User message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.Request.User;

      /**
       * Verifies a User message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a User message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns User
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.Request.User;

      /**
       * Creates a plain object from a User message. Also converts values to other types if specified.
       * @param message User
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.Request.User,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this User to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a ResponseGetAll. */
  interface IResponseGetAll {
    /** ResponseGetAll data */
    data?: Product.ResponseGetAll.IData[] | null;

    /** ResponseGetAll meta */
    meta?: Product.ResponseGetAll.IMeta | null;

    /** ResponseGetAll success */
    success?: boolean | null;

    /** ResponseGetAll message */
    message?: string | null;
  }

  /** Represents a ResponseGetAll. */
  class ResponseGetAll implements IResponseGetAll {
    /**
     * Constructs a new ResponseGetAll.
     * @param [properties] Properties to set
     */
    constructor(properties?: Product.IResponseGetAll);

    /** ResponseGetAll data. */
    public data: Product.ResponseGetAll.IData[];

    /** ResponseGetAll meta. */
    public meta?: Product.ResponseGetAll.IMeta | null;

    /** ResponseGetAll success. */
    public success: boolean;

    /** ResponseGetAll message. */
    public message: string;

    /**
     * Creates a new ResponseGetAll instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseGetAll instance
     */
    public static create(
      properties?: Product.IResponseGetAll,
    ): Product.ResponseGetAll;

    /**
     * Encodes the specified ResponseGetAll message. Does not implicitly {@link Product.ResponseGetAll.verify|verify} messages.
     * @param message ResponseGetAll message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Product.IResponseGetAll,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseGetAll message, length delimited. Does not implicitly {@link Product.ResponseGetAll.verify|verify} messages.
     * @param message ResponseGetAll message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Product.IResponseGetAll,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Product.ResponseGetAll;

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Product.ResponseGetAll;

    /**
     * Verifies a ResponseGetAll message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseGetAll message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseGetAll
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Product.ResponseGetAll;

    /**
     * Creates a plain object from a ResponseGetAll message. Also converts values to other types if specified.
     * @param message ResponseGetAll
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Product.ResponseGetAll,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseGetAll to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace ResponseGetAll {
    /** Properties of a Data. */
    interface IData {
      /** Data id */
      id?: string | null;

      /** Data productName */
      productName?: string | null;

      /** Data productLogo */
      productLogo?: string | null;

      /** Data productDescription */
      productDescription?: string | null;

      /** Data productShortDescription */
      productShortDescription?: string | null;

      /** Data metaTitleProduct */
      metaTitleProduct?: string | null;

      /** Data metaDescriptionProduct */
      metaDescriptionProduct?: string | null;

      /** Data productSlug */
      productSlug?: string | null;

      /** Data metaKeyword */
      metaKeyword?: string | null;

      /** Data price */
      price?: number | null;

      /** Data productImage */
      productImage?: Product.ResponseGetAll.Data.IProductimage[] | null;

      /** Data productCategory */
      productCategory?: Product.ResponseGetAll.Data.IProductcategory[] | null;

      /** Data createdBy */
      createdBy?: number | null;
    }

    /** Represents a Data. */
    class Data implements IData {
      /**
       * Constructs a new Data.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.ResponseGetAll.IData);

      /** Data id. */
      public id: string;

      /** Data productName. */
      public productName: string;

      /** Data productLogo. */
      public productLogo: string;

      /** Data productDescription. */
      public productDescription: string;

      /** Data productShortDescription. */
      public productShortDescription: string;

      /** Data metaTitleProduct. */
      public metaTitleProduct: string;

      /** Data metaDescriptionProduct. */
      public metaDescriptionProduct: string;

      /** Data productSlug. */
      public productSlug: string;

      /** Data metaKeyword. */
      public metaKeyword: string;

      /** Data price. */
      public price: number;

      /** Data productImage. */
      public productImage: Product.ResponseGetAll.Data.IProductimage[];

      /** Data productCategory. */
      public productCategory: Product.ResponseGetAll.Data.IProductcategory[];

      /** Data createdBy. */
      public createdBy: number;

      /**
       * Creates a new Data instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Data instance
       */
      public static create(
        properties?: Product.ResponseGetAll.IData,
      ): Product.ResponseGetAll.Data;

      /**
       * Encodes the specified Data message. Does not implicitly {@link Product.ResponseGetAll.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.ResponseGetAll.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.ResponseGetAll.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.ResponseGetAll.Data;

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.ResponseGetAll.Data;

      /**
       * Verifies a Data message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Data
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.ResponseGetAll.Data;

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @param message Data
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.ResponseGetAll.Data,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Data to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    namespace Data {
      /** Properties of a Productimage. */
      interface IProductimage {
        /** Productimage nameImage */
        nameImage?: string | null;

        /** Productimage altImage */
        altImage?: string | null;

        /** Productimage createdDate */
        createdDate?: string | null;

        /** Productimage updatedDate */
        updatedDate?: string | null;
      }

      /** Represents a Productimage. */
      class Productimage implements IProductimage {
        /**
         * Constructs a new Productimage.
         * @param [properties] Properties to set
         */
        constructor(properties?: Product.ResponseGetAll.Data.IProductimage);

        /** Productimage nameImage. */
        public nameImage: string;

        /** Productimage altImage. */
        public altImage: string;

        /** Productimage createdDate. */
        public createdDate: string;

        /** Productimage updatedDate. */
        public updatedDate: string;

        /**
         * Creates a new Productimage instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Productimage instance
         */
        public static create(
          properties?: Product.ResponseGetAll.Data.IProductimage,
        ): Product.ResponseGetAll.Data.Productimage;

        /**
         * Encodes the specified Productimage message. Does not implicitly {@link Product.ResponseGetAll.Data.Productimage.verify|verify} messages.
         * @param message Productimage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(
          message: Product.ResponseGetAll.Data.IProductimage,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Encodes the specified Productimage message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Data.Productimage.verify|verify} messages.
         * @param message Productimage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(
          message: Product.ResponseGetAll.Data.IProductimage,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Decodes a Productimage message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(
          reader: $protobuf.Reader | Uint8Array,
          length?: number,
        ): Product.ResponseGetAll.Data.Productimage;

        /**
         * Decodes a Productimage message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(
          reader: $protobuf.Reader | Uint8Array,
        ): Product.ResponseGetAll.Data.Productimage;

        /**
         * Verifies a Productimage message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): string | null;

        /**
         * Creates a Productimage message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Productimage
         */
        public static fromObject(object: {
          [k: string]: any;
        }): Product.ResponseGetAll.Data.Productimage;

        /**
         * Creates a plain object from a Productimage message. Also converts values to other types if specified.
         * @param message Productimage
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(
          message: Product.ResponseGetAll.Data.Productimage,
          options?: $protobuf.IConversionOptions,
        ): { [k: string]: any };

        /**
         * Converts this Productimage to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
      }

      /** Properties of a Productcategory. */
      interface IProductcategory {
        /** Productcategory categoryName */
        categoryName?: string | null;

        /** Productcategory categorySlug */
        categorySlug?: string | null;
      }

      /** Represents a Productcategory. */
      class Productcategory implements IProductcategory {
        /**
         * Constructs a new Productcategory.
         * @param [properties] Properties to set
         */
        constructor(properties?: Product.ResponseGetAll.Data.IProductcategory);

        /** Productcategory categoryName. */
        public categoryName: string;

        /** Productcategory categorySlug. */
        public categorySlug: string;

        /**
         * Creates a new Productcategory instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Productcategory instance
         */
        public static create(
          properties?: Product.ResponseGetAll.Data.IProductcategory,
        ): Product.ResponseGetAll.Data.Productcategory;

        /**
         * Encodes the specified Productcategory message. Does not implicitly {@link Product.ResponseGetAll.Data.Productcategory.verify|verify} messages.
         * @param message Productcategory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(
          message: Product.ResponseGetAll.Data.IProductcategory,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Encodes the specified Productcategory message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Data.Productcategory.verify|verify} messages.
         * @param message Productcategory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(
          message: Product.ResponseGetAll.Data.IProductcategory,
          writer?: $protobuf.Writer,
        ): $protobuf.Writer;

        /**
         * Decodes a Productcategory message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(
          reader: $protobuf.Reader | Uint8Array,
          length?: number,
        ): Product.ResponseGetAll.Data.Productcategory;

        /**
         * Decodes a Productcategory message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(
          reader: $protobuf.Reader | Uint8Array,
        ): Product.ResponseGetAll.Data.Productcategory;

        /**
         * Verifies a Productcategory message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): string | null;

        /**
         * Creates a Productcategory message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Productcategory
         */
        public static fromObject(object: {
          [k: string]: any;
        }): Product.ResponseGetAll.Data.Productcategory;

        /**
         * Creates a plain object from a Productcategory message. Also converts values to other types if specified.
         * @param message Productcategory
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(
          message: Product.ResponseGetAll.Data.Productcategory,
          options?: $protobuf.IConversionOptions,
        ): { [k: string]: any };

        /**
         * Converts this Productcategory to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
      }
    }

    /** Properties of a Meta. */
    interface IMeta {
      /** Meta pagination */
      pagination?: number | null;

      /** Meta limit */
      limit?: number | null;

      /** Meta totalPage */
      totalPage?: number | null;

      /** Meta count */
      count?: number | null;

      /** Meta lowerThan */
      lowerThan?: string | null;

      /** Meta greaterThan */
      greaterThan?: string | null;
    }

    /** Represents a Meta. */
    class Meta implements IMeta {
      /**
       * Constructs a new Meta.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.ResponseGetAll.IMeta);

      /** Meta pagination. */
      public pagination: number;

      /** Meta limit. */
      public limit: number;

      /** Meta totalPage. */
      public totalPage: number;

      /** Meta count. */
      public count: number;

      /** Meta lowerThan. */
      public lowerThan: string;

      /** Meta greaterThan. */
      public greaterThan: string;

      /**
       * Creates a new Meta instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Meta instance
       */
      public static create(
        properties?: Product.ResponseGetAll.IMeta,
      ): Product.ResponseGetAll.Meta;

      /**
       * Encodes the specified Meta message. Does not implicitly {@link Product.ResponseGetAll.Meta.verify|verify} messages.
       * @param message Meta message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.ResponseGetAll.IMeta,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Meta message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Meta.verify|verify} messages.
       * @param message Meta message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.ResponseGetAll.IMeta,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Meta message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.ResponseGetAll.Meta;

      /**
       * Decodes a Meta message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.ResponseGetAll.Meta;

      /**
       * Verifies a Meta message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Meta message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Meta
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.ResponseGetAll.Meta;

      /**
       * Creates a plain object from a Meta message. Also converts values to other types if specified.
       * @param message Meta
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.ResponseGetAll.Meta,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Meta to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a ResponseDetail. */
  interface IResponseDetail {
    /** ResponseDetail data */
    data?: Product.ResponseGetAll.IData | null;

    /** ResponseDetail success */
    success?: boolean | null;

    /** ResponseDetail message */
    message?: string | null;
  }

  /** Represents a ResponseDetail. */
  class ResponseDetail implements IResponseDetail {
    /**
     * Constructs a new ResponseDetail.
     * @param [properties] Properties to set
     */
    constructor(properties?: Product.IResponseDetail);

    /** ResponseDetail data. */
    public data?: Product.ResponseGetAll.IData | null;

    /** ResponseDetail success. */
    public success: boolean;

    /** ResponseDetail message. */
    public message: string;

    /**
     * Creates a new ResponseDetail instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseDetail instance
     */
    public static create(
      properties?: Product.IResponseDetail,
    ): Product.ResponseDetail;

    /**
     * Encodes the specified ResponseDetail message. Does not implicitly {@link Product.ResponseDetail.verify|verify} messages.
     * @param message ResponseDetail message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Product.IResponseDetail,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseDetail message, length delimited. Does not implicitly {@link Product.ResponseDetail.verify|verify} messages.
     * @param message ResponseDetail message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Product.IResponseDetail,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Product.ResponseDetail;

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Product.ResponseDetail;

    /**
     * Verifies a ResponseDetail message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseDetail message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseDetail
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Product.ResponseDetail;

    /**
     * Creates a plain object from a ResponseDetail message. Also converts values to other types if specified.
     * @param message ResponseDetail
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Product.ResponseDetail,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseDetail to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a ResponseCreate. */
  interface IResponseCreate {
    /** ResponseCreate data */
    data?: Product.ResponseGetAll.IData | null;

    /** ResponseCreate message */
    message?: string | null;

    /** ResponseCreate success */
    success?: boolean | null;
  }

  /** Represents a ResponseCreate. */
  class ResponseCreate implements IResponseCreate {
    /**
     * Constructs a new ResponseCreate.
     * @param [properties] Properties to set
     */
    constructor(properties?: Product.IResponseCreate);

    /** ResponseCreate data. */
    public data?: Product.ResponseGetAll.IData | null;

    /** ResponseCreate message. */
    public message: string;

    /** ResponseCreate success. */
    public success: boolean;

    /**
     * Creates a new ResponseCreate instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseCreate instance
     */
    public static create(
      properties?: Product.IResponseCreate,
    ): Product.ResponseCreate;

    /**
     * Encodes the specified ResponseCreate message. Does not implicitly {@link Product.ResponseCreate.verify|verify} messages.
     * @param message ResponseCreate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Product.IResponseCreate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseCreate message, length delimited. Does not implicitly {@link Product.ResponseCreate.verify|verify} messages.
     * @param message ResponseCreate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Product.IResponseCreate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Product.ResponseCreate;

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Product.ResponseCreate;

    /**
     * Verifies a ResponseCreate message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseCreate message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseCreate
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Product.ResponseCreate;

    /**
     * Creates a plain object from a ResponseCreate message. Also converts values to other types if specified.
     * @param message ResponseCreate
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Product.ResponseCreate,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseCreate to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a ResponseUpdate. */
  interface IResponseUpdate {
    /** ResponseUpdate data */
    data?: Product.ResponseGetAll.IData | null;

    /** ResponseUpdate success */
    success?: boolean | null;

    /** ResponseUpdate message */
    message?: string | null;
  }

  /** Represents a ResponseUpdate. */
  class ResponseUpdate implements IResponseUpdate {
    /**
     * Constructs a new ResponseUpdate.
     * @param [properties] Properties to set
     */
    constructor(properties?: Product.IResponseUpdate);

    /** ResponseUpdate data. */
    public data?: Product.ResponseGetAll.IData | null;

    /** ResponseUpdate success. */
    public success: boolean;

    /** ResponseUpdate message. */
    public message: string;

    /**
     * Creates a new ResponseUpdate instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseUpdate instance
     */
    public static create(
      properties?: Product.IResponseUpdate,
    ): Product.ResponseUpdate;

    /**
     * Encodes the specified ResponseUpdate message. Does not implicitly {@link Product.ResponseUpdate.verify|verify} messages.
     * @param message ResponseUpdate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Product.IResponseUpdate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseUpdate message, length delimited. Does not implicitly {@link Product.ResponseUpdate.verify|verify} messages.
     * @param message ResponseUpdate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Product.IResponseUpdate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Product.ResponseUpdate;

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Product.ResponseUpdate;

    /**
     * Verifies a ResponseUpdate message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseUpdate message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseUpdate
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Product.ResponseUpdate;

    /**
     * Creates a plain object from a ResponseUpdate message. Also converts values to other types if specified.
     * @param message ResponseUpdate
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Product.ResponseUpdate,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseUpdate to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a ResponseDelete. */
  interface IResponseDelete {
    /** ResponseDelete data */
    data?: Product.ResponseDelete.IData | null;

    /** ResponseDelete success */
    success?: boolean | null;

    /** ResponseDelete message */
    message?: string | null;
  }

  /** Represents a ResponseDelete. */
  class ResponseDelete implements IResponseDelete {
    /**
     * Constructs a new ResponseDelete.
     * @param [properties] Properties to set
     */
    constructor(properties?: Product.IResponseDelete);

    /** ResponseDelete data. */
    public data?: Product.ResponseDelete.IData | null;

    /** ResponseDelete success. */
    public success: boolean;

    /** ResponseDelete message. */
    public message: string;

    /**
     * Creates a new ResponseDelete instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseDelete instance
     */
    public static create(
      properties?: Product.IResponseDelete,
    ): Product.ResponseDelete;

    /**
     * Encodes the specified ResponseDelete message. Does not implicitly {@link Product.ResponseDelete.verify|verify} messages.
     * @param message ResponseDelete message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Product.IResponseDelete,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseDelete message, length delimited. Does not implicitly {@link Product.ResponseDelete.verify|verify} messages.
     * @param message ResponseDelete message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Product.IResponseDelete,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Product.ResponseDelete;

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Product.ResponseDelete;

    /**
     * Verifies a ResponseDelete message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseDelete message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseDelete
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Product.ResponseDelete;

    /**
     * Creates a plain object from a ResponseDelete message. Also converts values to other types if specified.
     * @param message ResponseDelete
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Product.ResponseDelete,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseDelete to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace ResponseDelete {
    /** Properties of a Data. */
    interface IData {
      /** Data message */
      message?: string | null;
    }

    /** Represents a Data. */
    class Data implements IData {
      /**
       * Constructs a new Data.
       * @param [properties] Properties to set
       */
      constructor(properties?: Product.ResponseDelete.IData);

      /** Data message. */
      public message: string;

      /**
       * Creates a new Data instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Data instance
       */
      public static create(
        properties?: Product.ResponseDelete.IData,
      ): Product.ResponseDelete.Data;

      /**
       * Encodes the specified Data message. Does not implicitly {@link Product.ResponseDelete.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Product.ResponseDelete.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Product.ResponseDelete.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Product.ResponseDelete.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Product.ResponseDelete.Data;

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Product.ResponseDelete.Data;

      /**
       * Verifies a Data message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Data
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Product.ResponseDelete.Data;

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @param message Data
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Product.ResponseDelete.Data,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Data to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }
}

/** Namespace google. */
export namespace google {
  /** Namespace protobuf. */
  namespace protobuf {
    /** Properties of an Any. */
    interface IAny {
      /** Any type_url */
      type_url?: string | null;

      /** Any value */
      value?: Uint8Array | null;
    }

    /** Represents an Any. */
    class Any implements IAny {
      /**
       * Constructs a new Any.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IAny);

      /** Any type_url. */
      public type_url: string;

      /** Any value. */
      public value: Uint8Array;

      /**
       * Creates a new Any instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Any instance
       */
      public static create(
        properties?: google.protobuf.IAny,
      ): google.protobuf.Any;

      /**
       * Encodes the specified Any message. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @param message Any message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IAny,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Any message, length delimited. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @param message Any message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IAny,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes an Any message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.Any;

      /**
       * Decodes an Any message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.Any;

      /**
       * Verifies an Any message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates an Any message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Any
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.Any;

      /**
       * Creates a plain object from an Any message. Also converts values to other types if specified.
       * @param message Any
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.Any,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Any to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Struct. */
    interface IStruct {
      /** Struct fields */
      fields?: { [k: string]: google.protobuf.IValue } | null;
    }

    /** Represents a Struct. */
    class Struct implements IStruct {
      /**
       * Constructs a new Struct.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IStruct);

      /** Struct fields. */
      public fields: { [k: string]: google.protobuf.IValue };

      /**
       * Creates a new Struct instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Struct instance
       */
      public static create(
        properties?: google.protobuf.IStruct,
      ): google.protobuf.Struct;

      /**
       * Encodes the specified Struct message. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @param message Struct message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IStruct,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Struct message, length delimited. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @param message Struct message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IStruct,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Struct message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.Struct;

      /**
       * Decodes a Struct message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.Struct;

      /**
       * Verifies a Struct message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Struct message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Struct
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.Struct;

      /**
       * Creates a plain object from a Struct message. Also converts values to other types if specified.
       * @param message Struct
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.Struct,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Struct to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Value. */
    interface IValue {
      /** Value nullValue */
      nullValue?: google.protobuf.NullValue | null;

      /** Value numberValue */
      numberValue?: number | null;

      /** Value stringValue */
      stringValue?: string | null;

      /** Value boolValue */
      boolValue?: boolean | null;

      /** Value structValue */
      structValue?: google.protobuf.IStruct | null;

      /** Value listValue */
      listValue?: google.protobuf.IListValue | null;
    }

    /** Represents a Value. */
    class Value implements IValue {
      /**
       * Constructs a new Value.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IValue);

      /** Value nullValue. */
      public nullValue?: google.protobuf.NullValue | null;

      /** Value numberValue. */
      public numberValue?: number | null;

      /** Value stringValue. */
      public stringValue?: string | null;

      /** Value boolValue. */
      public boolValue?: boolean | null;

      /** Value structValue. */
      public structValue?: google.protobuf.IStruct | null;

      /** Value listValue. */
      public listValue?: google.protobuf.IListValue | null;

      /** Value kind. */
      public kind?:
        | 'nullValue'
        | 'numberValue'
        | 'stringValue'
        | 'boolValue'
        | 'structValue'
        | 'listValue';

      /**
       * Creates a new Value instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Value instance
       */
      public static create(
        properties?: google.protobuf.IValue,
      ): google.protobuf.Value;

      /**
       * Encodes the specified Value message. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @param message Value message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Value message, length delimited. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @param message Value message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Value message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.Value;

      /**
       * Decodes a Value message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.Value;

      /**
       * Verifies a Value message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Value message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Value
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.Value;

      /**
       * Creates a plain object from a Value message. Also converts values to other types if specified.
       * @param message Value
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.Value,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Value to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** NullValue enum. */
    enum NullValue {
      NULL_VALUE = 0,
    }

    /** Properties of a ListValue. */
    interface IListValue {
      /** ListValue values */
      values?: google.protobuf.IValue[] | null;
    }

    /** Represents a ListValue. */
    class ListValue implements IListValue {
      /**
       * Constructs a new ListValue.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IListValue);

      /** ListValue values. */
      public values: google.protobuf.IValue[];

      /**
       * Creates a new ListValue instance using the specified properties.
       * @param [properties] Properties to set
       * @returns ListValue instance
       */
      public static create(
        properties?: google.protobuf.IListValue,
      ): google.protobuf.ListValue;

      /**
       * Encodes the specified ListValue message. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @param message ListValue message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IListValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified ListValue message, length delimited. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @param message ListValue message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IListValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a ListValue message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.ListValue;

      /**
       * Decodes a ListValue message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.ListValue;

      /**
       * Verifies a ListValue message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a ListValue message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns ListValue
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.ListValue;

      /**
       * Creates a plain object from a ListValue message. Also converts values to other types if specified.
       * @param message ListValue
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.ListValue,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this ListValue to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }
}
