/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
'use strict';

var $protobuf = require('protobufjs/minimal');

// Common aliases
var $Reader = $protobuf.Reader,
  $Writer = $protobuf.Writer,
  $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots['default'] || ($protobuf.roots['default'] = {});

$root.Catalogue = (function () {
  /**
   * Namespace Catalogue.
   * @exports Catalogue
   * @namespace
   */
  var Catalogue = {};

  Catalogue.Greeter = (function () {
    /**
     * Constructs a new Greeter service.
     * @memberof Catalogue
     * @classdesc Represents a Greeter
     * @extends $protobuf.rpc.Service
     * @constructor
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     */
    function Greeter(rpcImpl, requestDelimited, responseDelimited) {
      $protobuf.rpc.Service.call(
        this,
        rpcImpl,
        requestDelimited,
        responseDelimited,
      );
    }

    (Greeter.prototype = Object.create(
      $protobuf.rpc.Service.prototype,
    )).constructor = Greeter;

    /**
     * Creates new Greeter service using the specified rpc implementation.
     * @function create
     * @memberof Catalogue.Greeter
     * @static
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     * @returns {Greeter} RPC service. Useful where requests and/or responses are streamed.
     */
    Greeter.create = function create(
      rpcImpl,
      requestDelimited,
      responseDelimited,
    ) {
      return new this(rpcImpl, requestDelimited, responseDelimited);
    };

    /**
     * Callback as used by {@link Catalogue.Greeter#products}.
     * @memberof Catalogue.Greeter
     * @typedef productsCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Product.ResponseGetAll} [response] ResponseGetAll
     */

    /**
     * Calls products.
     * @function products
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @param {Catalogue.Greeter.productsCallback} callback Node-style callback called with the error, if any, and ResponseGetAll
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.products = function products(request, callback) {
        return this.rpcCall(
          products,
          $root.Product.Request,
          $root.Product.ResponseGetAll,
          request,
          callback,
        );
      }),
      'name',
      { value: 'products' },
    );

    /**
     * Calls products.
     * @function products
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @returns {Promise<Product.ResponseGetAll>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Catalogue.Greeter#product}.
     * @memberof Catalogue.Greeter
     * @typedef productCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Product.ResponseDetail} [response] ResponseDetail
     */

    /**
     * Calls product.
     * @function product
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @param {Catalogue.Greeter.productCallback} callback Node-style callback called with the error, if any, and ResponseDetail
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.product = function product(request, callback) {
        return this.rpcCall(
          product,
          $root.Product.Request,
          $root.Product.ResponseDetail,
          request,
          callback,
        );
      }),
      'name',
      { value: 'product' },
    );

    /**
     * Calls product.
     * @function product
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @returns {Promise<Product.ResponseDetail>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Catalogue.Greeter#productCreate}.
     * @memberof Catalogue.Greeter
     * @typedef productCreateCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Product.ResponseCreate} [response] ResponseCreate
     */

    /**
     * Calls productCreate.
     * @function productCreate
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @param {Catalogue.Greeter.productCreateCallback} callback Node-style callback called with the error, if any, and ResponseCreate
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.productCreate = function productCreate(
        request,
        callback,
      ) {
        return this.rpcCall(
          productCreate,
          $root.Product.Request,
          $root.Product.ResponseCreate,
          request,
          callback,
        );
      }),
      'name',
      { value: 'productCreate' },
    );

    /**
     * Calls productCreate.
     * @function productCreate
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @returns {Promise<Product.ResponseCreate>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Catalogue.Greeter#productUpdate}.
     * @memberof Catalogue.Greeter
     * @typedef productUpdateCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Product.ResponseUpdate} [response] ResponseUpdate
     */

    /**
     * Calls productUpdate.
     * @function productUpdate
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @param {Catalogue.Greeter.productUpdateCallback} callback Node-style callback called with the error, if any, and ResponseUpdate
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.productUpdate = function productUpdate(
        request,
        callback,
      ) {
        return this.rpcCall(
          productUpdate,
          $root.Product.Request,
          $root.Product.ResponseUpdate,
          request,
          callback,
        );
      }),
      'name',
      { value: 'productUpdate' },
    );

    /**
     * Calls productUpdate.
     * @function productUpdate
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @returns {Promise<Product.ResponseUpdate>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link Catalogue.Greeter#productDelete}.
     * @memberof Catalogue.Greeter
     * @typedef productDeleteCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Product.ResponseDelete} [response] ResponseDelete
     */

    /**
     * Calls productDelete.
     * @function productDelete
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @param {Catalogue.Greeter.productDeleteCallback} callback Node-style callback called with the error, if any, and ResponseDelete
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.productDelete = function productDelete(
        request,
        callback,
      ) {
        return this.rpcCall(
          productDelete,
          $root.Product.Request,
          $root.Product.ResponseDelete,
          request,
          callback,
        );
      }),
      'name',
      { value: 'productDelete' },
    );

    /**
     * Calls productDelete.
     * @function productDelete
     * @memberof Catalogue.Greeter
     * @instance
     * @param {Product.IRequest} request Request message or plain object
     * @returns {Promise<Product.ResponseDelete>} Promise
     * @variation 2
     */

    return Greeter;
  })();

  return Catalogue;
})();

$root.Product = (function () {
  /**
   * Namespace Product.
   * @exports Product
   * @namespace
   */
  var Product = {};

  Product.Request = (function () {
    /**
     * Properties of a Request.
     * @memberof Product
     * @interface IRequest
     * @property {string|null} [authorization] Request authorization
     * @property {Product.Request.IQuery|null} [query] Request query
     * @property {Product.Request.IBody|null} [body] Request body
     * @property {Product.Request.IParams|null} [params] Request params
     * @property {Product.Request.IUser|null} [user] Request user
     */

    /**
     * Constructs a new Request.
     * @memberof Product
     * @classdesc Represents a Request.
     * @implements IRequest
     * @constructor
     * @param {Product.IRequest=} [properties] Properties to set
     */
    function Request(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * Request authorization.
     * @member {string|null|undefined} authorization
     * @memberof Product.Request
     * @instance
     */
    Request.prototype.authorization = null;

    /**
     * Request query.
     * @member {Product.Request.IQuery|null|undefined} query
     * @memberof Product.Request
     * @instance
     */
    Request.prototype.query = null;

    /**
     * Request body.
     * @member {Product.Request.IBody|null|undefined} body
     * @memberof Product.Request
     * @instance
     */
    Request.prototype.body = null;

    /**
     * Request params.
     * @member {Product.Request.IParams|null|undefined} params
     * @memberof Product.Request
     * @instance
     */
    Request.prototype.params = null;

    /**
     * Request user.
     * @member {Product.Request.IUser|null|undefined} user
     * @memberof Product.Request
     * @instance
     */
    Request.prototype.user = null;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * Request _authorization.
     * @member {"authorization"|undefined} _authorization
     * @memberof Product.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_authorization', {
      get: $util.oneOfGetter(($oneOfFields = ['authorization'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _query.
     * @member {"query"|undefined} _query
     * @memberof Product.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_query', {
      get: $util.oneOfGetter(($oneOfFields = ['query'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _body.
     * @member {"body"|undefined} _body
     * @memberof Product.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_body', {
      get: $util.oneOfGetter(($oneOfFields = ['body'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _params.
     * @member {"params"|undefined} _params
     * @memberof Product.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_params', {
      get: $util.oneOfGetter(($oneOfFields = ['params'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _user.
     * @member {"user"|undefined} _user
     * @memberof Product.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_user', {
      get: $util.oneOfGetter(($oneOfFields = ['user'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Creates a new Request instance using the specified properties.
     * @function create
     * @memberof Product.Request
     * @static
     * @param {Product.IRequest=} [properties] Properties to set
     * @returns {Product.Request} Request instance
     */
    Request.create = function create(properties) {
      return new Request(properties);
    };

    /**
     * Encodes the specified Request message. Does not implicitly {@link Product.Request.verify|verify} messages.
     * @function encode
     * @memberof Product.Request
     * @static
     * @param {Product.IRequest} message Request message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Request.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (
        message.authorization != null &&
        Object.hasOwnProperty.call(message, 'authorization')
      )
        writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.authorization);
      if (message.query != null && Object.hasOwnProperty.call(message, 'query'))
        $root.Product.Request.Query.encode(
          message.query,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (message.body != null && Object.hasOwnProperty.call(message, 'body'))
        $root.Product.Request.Body.encode(
          message.body,
          writer.uint32(/* id 3, wireType 2 =*/ 26).fork(),
        ).ldelim();
      if (
        message.params != null &&
        Object.hasOwnProperty.call(message, 'params')
      )
        $root.Product.Request.Params.encode(
          message.params,
          writer.uint32(/* id 4, wireType 2 =*/ 34).fork(),
        ).ldelim();
      if (message.user != null && Object.hasOwnProperty.call(message, 'user'))
        $root.Product.Request.User.encode(
          message.user,
          writer.uint32(/* id 5, wireType 2 =*/ 42).fork(),
        ).ldelim();
      return writer;
    };

    /**
     * Encodes the specified Request message, length delimited. Does not implicitly {@link Product.Request.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Product.Request
     * @static
     * @param {Product.IRequest} message Request message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Request.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Request message from the specified reader or buffer.
     * @function decode
     * @memberof Product.Request
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Product.Request} Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Request.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Product.Request();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.authorization = reader.string();
            break;
          case 2:
            message.query = $root.Product.Request.Query.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.body = $root.Product.Request.Body.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 4:
            message.params = $root.Product.Request.Params.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 5:
            message.user = $root.Product.Request.User.decode(
              reader,
              reader.uint32(),
            );
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a Request message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Product.Request
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Product.Request} Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Request.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Request message.
     * @function verify
     * @memberof Product.Request
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Request.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      var properties = {};
      if (
        message.authorization != null &&
        message.hasOwnProperty('authorization')
      ) {
        properties._authorization = 1;
        if (!$util.isString(message.authorization))
          return 'authorization: string expected';
      }
      if (message.query != null && message.hasOwnProperty('query')) {
        properties._query = 1;
        {
          var error = $root.Product.Request.Query.verify(message.query);
          if (error) return 'query.' + error;
        }
      }
      if (message.body != null && message.hasOwnProperty('body')) {
        properties._body = 1;
        {
          var error = $root.Product.Request.Body.verify(message.body);
          if (error) return 'body.' + error;
        }
      }
      if (message.params != null && message.hasOwnProperty('params')) {
        properties._params = 1;
        {
          var error = $root.Product.Request.Params.verify(message.params);
          if (error) return 'params.' + error;
        }
      }
      if (message.user != null && message.hasOwnProperty('user')) {
        properties._user = 1;
        {
          var error = $root.Product.Request.User.verify(message.user);
          if (error) return 'user.' + error;
        }
      }
      return null;
    };

    /**
     * Creates a Request message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Product.Request
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Product.Request} Request
     */
    Request.fromObject = function fromObject(object) {
      if (object instanceof $root.Product.Request) return object;
      var message = new $root.Product.Request();
      if (object.authorization != null)
        message.authorization = String(object.authorization);
      if (object.query != null) {
        if (typeof object.query !== 'object')
          throw TypeError('.Product.Request.query: object expected');
        message.query = $root.Product.Request.Query.fromObject(object.query);
      }
      if (object.body != null) {
        if (typeof object.body !== 'object')
          throw TypeError('.Product.Request.body: object expected');
        message.body = $root.Product.Request.Body.fromObject(object.body);
      }
      if (object.params != null) {
        if (typeof object.params !== 'object')
          throw TypeError('.Product.Request.params: object expected');
        message.params = $root.Product.Request.Params.fromObject(object.params);
      }
      if (object.user != null) {
        if (typeof object.user !== 'object')
          throw TypeError('.Product.Request.user: object expected');
        message.user = $root.Product.Request.User.fromObject(object.user);
      }
      return message;
    };

    /**
     * Creates a plain object from a Request message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Product.Request
     * @static
     * @param {Product.Request} message Request
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Request.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (
        message.authorization != null &&
        message.hasOwnProperty('authorization')
      ) {
        object.authorization = message.authorization;
        if (options.oneofs) object._authorization = 'authorization';
      }
      if (message.query != null && message.hasOwnProperty('query')) {
        object.query = $root.Product.Request.Query.toObject(
          message.query,
          options,
        );
        if (options.oneofs) object._query = 'query';
      }
      if (message.body != null && message.hasOwnProperty('body')) {
        object.body = $root.Product.Request.Body.toObject(
          message.body,
          options,
        );
        if (options.oneofs) object._body = 'body';
      }
      if (message.params != null && message.hasOwnProperty('params')) {
        object.params = $root.Product.Request.Params.toObject(
          message.params,
          options,
        );
        if (options.oneofs) object._params = 'params';
      }
      if (message.user != null && message.hasOwnProperty('user')) {
        object.user = $root.Product.Request.User.toObject(
          message.user,
          options,
        );
        if (options.oneofs) object._user = 'user';
      }
      return object;
    };

    /**
     * Converts this Request to JSON.
     * @function toJSON
     * @memberof Product.Request
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Request.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    Request.Query = (function () {
      /**
       * Properties of a Query.
       * @memberof Product.Request
       * @interface IQuery
       * @property {number|null} [pagination] Query pagination
       * @property {number|null} [limit] Query limit
       * @property {string|null} [filterBy] Query filterBy
       * @property {string|null} [search] Query search
       * @property {string|null} [lowerThan] Query lowerThan
       * @property {string|null} [greaterThan] Query greaterThan
       * @property {Array.<number>|null} [ids] Query ids
       */

      /**
       * Constructs a new Query.
       * @memberof Product.Request
       * @classdesc Represents a Query.
       * @implements IQuery
       * @constructor
       * @param {Product.Request.IQuery=} [properties] Properties to set
       */
      function Query(properties) {
        this.ids = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Query pagination.
       * @member {number|null|undefined} pagination
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.pagination = null;

      /**
       * Query limit.
       * @member {number|null|undefined} limit
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.limit = null;

      /**
       * Query filterBy.
       * @member {string|null|undefined} filterBy
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.filterBy = null;

      /**
       * Query search.
       * @member {string|null|undefined} search
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.search = null;

      /**
       * Query lowerThan.
       * @member {string|null|undefined} lowerThan
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.lowerThan = null;

      /**
       * Query greaterThan.
       * @member {string|null|undefined} greaterThan
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.greaterThan = null;

      /**
       * Query ids.
       * @member {Array.<number>} ids
       * @memberof Product.Request.Query
       * @instance
       */
      Query.prototype.ids = $util.emptyArray;

      // OneOf field names bound to virtual getters and setters
      var $oneOfFields;

      /**
       * Query _pagination.
       * @member {"pagination"|undefined} _pagination
       * @memberof Product.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_pagination', {
        get: $util.oneOfGetter(($oneOfFields = ['pagination'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _limit.
       * @member {"limit"|undefined} _limit
       * @memberof Product.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_limit', {
        get: $util.oneOfGetter(($oneOfFields = ['limit'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _filterBy.
       * @member {"filterBy"|undefined} _filterBy
       * @memberof Product.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_filterBy', {
        get: $util.oneOfGetter(($oneOfFields = ['filterBy'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _search.
       * @member {"search"|undefined} _search
       * @memberof Product.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_search', {
        get: $util.oneOfGetter(($oneOfFields = ['search'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _lowerThan.
       * @member {"lowerThan"|undefined} _lowerThan
       * @memberof Product.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_lowerThan', {
        get: $util.oneOfGetter(($oneOfFields = ['lowerThan'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _greaterThan.
       * @member {"greaterThan"|undefined} _greaterThan
       * @memberof Product.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_greaterThan', {
        get: $util.oneOfGetter(($oneOfFields = ['greaterThan'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Creates a new Query instance using the specified properties.
       * @function create
       * @memberof Product.Request.Query
       * @static
       * @param {Product.Request.IQuery=} [properties] Properties to set
       * @returns {Product.Request.Query} Query instance
       */
      Query.create = function create(properties) {
        return new Query(properties);
      };

      /**
       * Encodes the specified Query message. Does not implicitly {@link Product.Request.Query.verify|verify} messages.
       * @function encode
       * @memberof Product.Request.Query
       * @static
       * @param {Product.Request.IQuery} message Query message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Query.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.pagination != null &&
          Object.hasOwnProperty.call(message, 'pagination')
        )
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.pagination);
        if (
          message.limit != null &&
          Object.hasOwnProperty.call(message, 'limit')
        )
          writer.uint32(/* id 2, wireType 0 =*/ 16).int32(message.limit);
        if (
          message.filterBy != null &&
          Object.hasOwnProperty.call(message, 'filterBy')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.filterBy);
        if (
          message.search != null &&
          Object.hasOwnProperty.call(message, 'search')
        )
          writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.search);
        if (
          message.lowerThan != null &&
          Object.hasOwnProperty.call(message, 'lowerThan')
        )
          writer.uint32(/* id 5, wireType 2 =*/ 42).string(message.lowerThan);
        if (
          message.greaterThan != null &&
          Object.hasOwnProperty.call(message, 'greaterThan')
        )
          writer.uint32(/* id 6, wireType 2 =*/ 50).string(message.greaterThan);
        if (message.ids != null && message.ids.length) {
          writer.uint32(/* id 7, wireType 2 =*/ 58).fork();
          for (var i = 0; i < message.ids.length; ++i)
            writer.int32(message.ids[i]);
          writer.ldelim();
        }
        return writer;
      };

      /**
       * Encodes the specified Query message, length delimited. Does not implicitly {@link Product.Request.Query.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.Request.Query
       * @static
       * @param {Product.Request.IQuery} message Query message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Query.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Query message from the specified reader or buffer.
       * @function decode
       * @memberof Product.Request.Query
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.Request.Query} Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Query.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.Request.Query();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.pagination = reader.int32();
              break;
            case 2:
              message.limit = reader.int32();
              break;
            case 3:
              message.filterBy = reader.string();
              break;
            case 4:
              message.search = reader.string();
              break;
            case 5:
              message.lowerThan = reader.string();
              break;
            case 6:
              message.greaterThan = reader.string();
              break;
            case 7:
              if (!(message.ids && message.ids.length)) message.ids = [];
              if ((tag & 7) === 2) {
                var end2 = reader.uint32() + reader.pos;
                while (reader.pos < end2) message.ids.push(reader.int32());
              } else message.ids.push(reader.int32());
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Query message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.Request.Query
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.Request.Query} Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Query.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Query message.
       * @function verify
       * @memberof Product.Request.Query
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Query.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        var properties = {};
        if (
          message.pagination != null &&
          message.hasOwnProperty('pagination')
        ) {
          properties._pagination = 1;
          if (!$util.isInteger(message.pagination))
            return 'pagination: integer expected';
        }
        if (message.limit != null && message.hasOwnProperty('limit')) {
          properties._limit = 1;
          if (!$util.isInteger(message.limit)) return 'limit: integer expected';
        }
        if (message.filterBy != null && message.hasOwnProperty('filterBy')) {
          properties._filterBy = 1;
          if (!$util.isString(message.filterBy))
            return 'filterBy: string expected';
        }
        if (message.search != null && message.hasOwnProperty('search')) {
          properties._search = 1;
          if (!$util.isString(message.search)) return 'search: string expected';
        }
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan')) {
          properties._lowerThan = 1;
          if (!$util.isString(message.lowerThan))
            return 'lowerThan: string expected';
        }
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        ) {
          properties._greaterThan = 1;
          if (!$util.isString(message.greaterThan))
            return 'greaterThan: string expected';
        }
        if (message.ids != null && message.hasOwnProperty('ids')) {
          if (!Array.isArray(message.ids)) return 'ids: array expected';
          for (var i = 0; i < message.ids.length; ++i)
            if (!$util.isInteger(message.ids[i]))
              return 'ids: integer[] expected';
        }
        return null;
      };

      /**
       * Creates a Query message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.Request.Query
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.Request.Query} Query
       */
      Query.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.Request.Query) return object;
        var message = new $root.Product.Request.Query();
        if (object.pagination != null)
          message.pagination = object.pagination | 0;
        if (object.limit != null) message.limit = object.limit | 0;
        if (object.filterBy != null) message.filterBy = String(object.filterBy);
        if (object.search != null) message.search = String(object.search);
        if (object.lowerThan != null)
          message.lowerThan = String(object.lowerThan);
        if (object.greaterThan != null)
          message.greaterThan = String(object.greaterThan);
        if (object.ids) {
          if (!Array.isArray(object.ids))
            throw TypeError('.Product.Request.Query.ids: array expected');
          message.ids = [];
          for (var i = 0; i < object.ids.length; ++i)
            message.ids[i] = object.ids[i] | 0;
        }
        return message;
      };

      /**
       * Creates a plain object from a Query message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.Request.Query
       * @static
       * @param {Product.Request.Query} message Query
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Query.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.ids = [];
        if (
          message.pagination != null &&
          message.hasOwnProperty('pagination')
        ) {
          object.pagination = message.pagination;
          if (options.oneofs) object._pagination = 'pagination';
        }
        if (message.limit != null && message.hasOwnProperty('limit')) {
          object.limit = message.limit;
          if (options.oneofs) object._limit = 'limit';
        }
        if (message.filterBy != null && message.hasOwnProperty('filterBy')) {
          object.filterBy = message.filterBy;
          if (options.oneofs) object._filterBy = 'filterBy';
        }
        if (message.search != null && message.hasOwnProperty('search')) {
          object.search = message.search;
          if (options.oneofs) object._search = 'search';
        }
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan')) {
          object.lowerThan = message.lowerThan;
          if (options.oneofs) object._lowerThan = 'lowerThan';
        }
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        ) {
          object.greaterThan = message.greaterThan;
          if (options.oneofs) object._greaterThan = 'greaterThan';
        }
        if (message.ids && message.ids.length) {
          object.ids = [];
          for (var j = 0; j < message.ids.length; ++j)
            object.ids[j] = message.ids[j];
        }
        return object;
      };

      /**
       * Converts this Query to JSON.
       * @function toJSON
       * @memberof Product.Request.Query
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Query.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Query;
    })();

    Request.Body = (function () {
      /**
       * Properties of a Body.
       * @memberof Product.Request
       * @interface IBody
       * @property {string|null} [productName] Body productName
       * @property {string|null} [productLogo] Body productLogo
       * @property {string|null} [productDescription] Body productDescription
       * @property {string|null} [productShortDescription] Body productShortDescription
       * @property {string|null} [metaTitleProduct] Body metaTitleProduct
       * @property {string|null} [metaDescriptionProduct] Body metaDescriptionProduct
       * @property {string|null} [productSlug] Body productSlug
       * @property {string|null} [metaKeyword] Body metaKeyword
       * @property {number|null} [price] Body price
       * @property {number|null} [createdBy] Body createdBy
       * @property {Array.<Product.Request.Body.IProductimage>|null} [productImage] Body productImage
       * @property {Array.<Product.Request.Body.IProductcategory>|null} [productCategory] Body productCategory
       */

      /**
       * Constructs a new Body.
       * @memberof Product.Request
       * @classdesc Represents a Body.
       * @implements IBody
       * @constructor
       * @param {Product.Request.IBody=} [properties] Properties to set
       */
      function Body(properties) {
        this.productImage = [];
        this.productCategory = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Body productName.
       * @member {string} productName
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productName = '';

      /**
       * Body productLogo.
       * @member {string} productLogo
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productLogo = '';

      /**
       * Body productDescription.
       * @member {string} productDescription
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productDescription = '';

      /**
       * Body productShortDescription.
       * @member {string} productShortDescription
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productShortDescription = '';

      /**
       * Body metaTitleProduct.
       * @member {string} metaTitleProduct
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.metaTitleProduct = '';

      /**
       * Body metaDescriptionProduct.
       * @member {string} metaDescriptionProduct
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.metaDescriptionProduct = '';

      /**
       * Body productSlug.
       * @member {string} productSlug
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productSlug = '';

      /**
       * Body metaKeyword.
       * @member {string} metaKeyword
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.metaKeyword = '';

      /**
       * Body price.
       * @member {number} price
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.price = 0;

      /**
       * Body createdBy.
       * @member {number} createdBy
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.createdBy = 0;

      /**
       * Body productImage.
       * @member {Array.<Product.Request.Body.IProductimage>} productImage
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productImage = $util.emptyArray;

      /**
       * Body productCategory.
       * @member {Array.<Product.Request.Body.IProductcategory>} productCategory
       * @memberof Product.Request.Body
       * @instance
       */
      Body.prototype.productCategory = $util.emptyArray;

      /**
       * Creates a new Body instance using the specified properties.
       * @function create
       * @memberof Product.Request.Body
       * @static
       * @param {Product.Request.IBody=} [properties] Properties to set
       * @returns {Product.Request.Body} Body instance
       */
      Body.create = function create(properties) {
        return new Body(properties);
      };

      /**
       * Encodes the specified Body message. Does not implicitly {@link Product.Request.Body.verify|verify} messages.
       * @function encode
       * @memberof Product.Request.Body
       * @static
       * @param {Product.Request.IBody} message Body message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Body.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.productName != null &&
          Object.hasOwnProperty.call(message, 'productName')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.productName);
        if (
          message.productLogo != null &&
          Object.hasOwnProperty.call(message, 'productLogo')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.productLogo);
        if (
          message.productDescription != null &&
          Object.hasOwnProperty.call(message, 'productDescription')
        )
          writer
            .uint32(/* id 4, wireType 2 =*/ 34)
            .string(message.productDescription);
        if (
          message.productShortDescription != null &&
          Object.hasOwnProperty.call(message, 'productShortDescription')
        )
          writer
            .uint32(/* id 5, wireType 2 =*/ 42)
            .string(message.productShortDescription);
        if (
          message.metaTitleProduct != null &&
          Object.hasOwnProperty.call(message, 'metaTitleProduct')
        )
          writer
            .uint32(/* id 6, wireType 2 =*/ 50)
            .string(message.metaTitleProduct);
        if (
          message.metaDescriptionProduct != null &&
          Object.hasOwnProperty.call(message, 'metaDescriptionProduct')
        )
          writer
            .uint32(/* id 7, wireType 2 =*/ 58)
            .string(message.metaDescriptionProduct);
        if (
          message.productSlug != null &&
          Object.hasOwnProperty.call(message, 'productSlug')
        )
          writer.uint32(/* id 8, wireType 2 =*/ 66).string(message.productSlug);
        if (
          message.metaKeyword != null &&
          Object.hasOwnProperty.call(message, 'metaKeyword')
        )
          writer.uint32(/* id 9, wireType 2 =*/ 74).string(message.metaKeyword);
        if (
          message.price != null &&
          Object.hasOwnProperty.call(message, 'price')
        )
          writer.uint32(/* id 10, wireType 0 =*/ 80).uint32(message.price);
        if (message.productImage != null && message.productImage.length)
          for (var i = 0; i < message.productImage.length; ++i)
            $root.Product.Request.Body.Productimage.encode(
              message.productImage[i],
              writer.uint32(/* id 11, wireType 2 =*/ 90).fork(),
            ).ldelim();
        if (message.productCategory != null && message.productCategory.length)
          for (var i = 0; i < message.productCategory.length; ++i)
            $root.Product.Request.Body.Productcategory.encode(
              message.productCategory[i],
              writer.uint32(/* id 12, wireType 2 =*/ 98).fork(),
            ).ldelim();
        if (
          message.createdBy != null &&
          Object.hasOwnProperty.call(message, 'createdBy')
        )
          writer.uint32(/* id 13, wireType 0 =*/ 104).uint32(message.createdBy);
        return writer;
      };

      /**
       * Encodes the specified Body message, length delimited. Does not implicitly {@link Product.Request.Body.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.Request.Body
       * @static
       * @param {Product.Request.IBody} message Body message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Body.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Body message from the specified reader or buffer.
       * @function decode
       * @memberof Product.Request.Body
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.Request.Body} Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Body.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.Request.Body();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 2:
              message.productName = reader.string();
              break;
            case 3:
              message.productLogo = reader.string();
              break;
            case 4:
              message.productDescription = reader.string();
              break;
            case 5:
              message.productShortDescription = reader.string();
              break;
            case 6:
              message.metaTitleProduct = reader.string();
              break;
            case 7:
              message.metaDescriptionProduct = reader.string();
              break;
            case 8:
              message.productSlug = reader.string();
              break;
            case 9:
              message.metaKeyword = reader.string();
              break;
            case 10:
              message.price = reader.uint32();
              break;
            case 13:
              message.createdBy = reader.uint32();
              break;
            case 11:
              if (!(message.productImage && message.productImage.length))
                message.productImage = [];
              message.productImage.push(
                $root.Product.Request.Body.Productimage.decode(
                  reader,
                  reader.uint32(),
                ),
              );
              break;
            case 12:
              if (!(message.productCategory && message.productCategory.length))
                message.productCategory = [];
              message.productCategory.push(
                $root.Product.Request.Body.Productcategory.decode(
                  reader,
                  reader.uint32(),
                ),
              );
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Body message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.Request.Body
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.Request.Body} Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Body.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Body message.
       * @function verify
       * @memberof Product.Request.Body
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Body.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (
          message.productName != null &&
          message.hasOwnProperty('productName')
        )
          if (!$util.isString(message.productName))
            return 'productName: string expected';
        if (
          message.productLogo != null &&
          message.hasOwnProperty('productLogo')
        )
          if (!$util.isString(message.productLogo))
            return 'productLogo: string expected';
        if (
          message.productDescription != null &&
          message.hasOwnProperty('productDescription')
        )
          if (!$util.isString(message.productDescription))
            return 'productDescription: string expected';
        if (
          message.productShortDescription != null &&
          message.hasOwnProperty('productShortDescription')
        )
          if (!$util.isString(message.productShortDescription))
            return 'productShortDescription: string expected';
        if (
          message.metaTitleProduct != null &&
          message.hasOwnProperty('metaTitleProduct')
        )
          if (!$util.isString(message.metaTitleProduct))
            return 'metaTitleProduct: string expected';
        if (
          message.metaDescriptionProduct != null &&
          message.hasOwnProperty('metaDescriptionProduct')
        )
          if (!$util.isString(message.metaDescriptionProduct))
            return 'metaDescriptionProduct: string expected';
        if (
          message.productSlug != null &&
          message.hasOwnProperty('productSlug')
        )
          if (!$util.isString(message.productSlug))
            return 'productSlug: string expected';
        if (
          message.metaKeyword != null &&
          message.hasOwnProperty('metaKeyword')
        )
          if (!$util.isString(message.metaKeyword))
            return 'metaKeyword: string expected';
        if (message.price != null && message.hasOwnProperty('price'))
          if (!$util.isInteger(message.price)) return 'price: integer expected';
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          if (!$util.isInteger(message.createdBy))
            return 'createdBy: integer expected';
        if (
          message.productImage != null &&
          message.hasOwnProperty('productImage')
        ) {
          if (!Array.isArray(message.productImage))
            return 'productImage: array expected';
          for (var i = 0; i < message.productImage.length; ++i) {
            var error = $root.Product.Request.Body.Productimage.verify(
              message.productImage[i],
            );
            if (error) return 'productImage.' + error;
          }
        }
        if (
          message.productCategory != null &&
          message.hasOwnProperty('productCategory')
        ) {
          if (!Array.isArray(message.productCategory))
            return 'productCategory: array expected';
          for (var i = 0; i < message.productCategory.length; ++i) {
            var error = $root.Product.Request.Body.Productcategory.verify(
              message.productCategory[i],
            );
            if (error) return 'productCategory.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a Body message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.Request.Body
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.Request.Body} Body
       */
      Body.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.Request.Body) return object;
        var message = new $root.Product.Request.Body();
        if (object.productName != null)
          message.productName = String(object.productName);
        if (object.productLogo != null)
          message.productLogo = String(object.productLogo);
        if (object.productDescription != null)
          message.productDescription = String(object.productDescription);
        if (object.productShortDescription != null)
          message.productShortDescription = String(
            object.productShortDescription,
          );
        if (object.metaTitleProduct != null)
          message.metaTitleProduct = String(object.metaTitleProduct);
        if (object.metaDescriptionProduct != null)
          message.metaDescriptionProduct = String(
            object.metaDescriptionProduct,
          );
        if (object.productSlug != null)
          message.productSlug = String(object.productSlug);
        if (object.metaKeyword != null)
          message.metaKeyword = String(object.metaKeyword);
        if (object.price != null) message.price = object.price >>> 0;
        if (object.createdBy != null)
          message.createdBy = object.createdBy >>> 0;
        if (object.productImage) {
          if (!Array.isArray(object.productImage))
            throw TypeError(
              '.Product.Request.Body.productImage: array expected',
            );
          message.productImage = [];
          for (var i = 0; i < object.productImage.length; ++i) {
            if (typeof object.productImage[i] !== 'object')
              throw TypeError(
                '.Product.Request.Body.productImage: object expected',
              );
            message.productImage[i] =
              $root.Product.Request.Body.Productimage.fromObject(
                object.productImage[i],
              );
          }
        }
        if (object.productCategory) {
          if (!Array.isArray(object.productCategory))
            throw TypeError(
              '.Product.Request.Body.productCategory: array expected',
            );
          message.productCategory = [];
          for (var i = 0; i < object.productCategory.length; ++i) {
            if (typeof object.productCategory[i] !== 'object')
              throw TypeError(
                '.Product.Request.Body.productCategory: object expected',
              );
            message.productCategory[i] =
              $root.Product.Request.Body.Productcategory.fromObject(
                object.productCategory[i],
              );
          }
        }
        return message;
      };

      /**
       * Creates a plain object from a Body message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.Request.Body
       * @static
       * @param {Product.Request.Body} message Body
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Body.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) {
          object.productImage = [];
          object.productCategory = [];
        }
        if (options.defaults) {
          object.productName = '';
          object.productLogo = '';
          object.productDescription = '';
          object.productShortDescription = '';
          object.metaTitleProduct = '';
          object.metaDescriptionProduct = '';
          object.productSlug = '';
          object.metaKeyword = '';
          object.price = 0;
          object.createdBy = 0;
        }
        if (
          message.productName != null &&
          message.hasOwnProperty('productName')
        )
          object.productName = message.productName;
        if (
          message.productLogo != null &&
          message.hasOwnProperty('productLogo')
        )
          object.productLogo = message.productLogo;
        if (
          message.productDescription != null &&
          message.hasOwnProperty('productDescription')
        )
          object.productDescription = message.productDescription;
        if (
          message.productShortDescription != null &&
          message.hasOwnProperty('productShortDescription')
        )
          object.productShortDescription = message.productShortDescription;
        if (
          message.metaTitleProduct != null &&
          message.hasOwnProperty('metaTitleProduct')
        )
          object.metaTitleProduct = message.metaTitleProduct;
        if (
          message.metaDescriptionProduct != null &&
          message.hasOwnProperty('metaDescriptionProduct')
        )
          object.metaDescriptionProduct = message.metaDescriptionProduct;
        if (
          message.productSlug != null &&
          message.hasOwnProperty('productSlug')
        )
          object.productSlug = message.productSlug;
        if (
          message.metaKeyword != null &&
          message.hasOwnProperty('metaKeyword')
        )
          object.metaKeyword = message.metaKeyword;
        if (message.price != null && message.hasOwnProperty('price'))
          object.price = message.price;
        if (message.productImage && message.productImage.length) {
          object.productImage = [];
          for (var j = 0; j < message.productImage.length; ++j)
            object.productImage[j] =
              $root.Product.Request.Body.Productimage.toObject(
                message.productImage[j],
                options,
              );
        }
        if (message.productCategory && message.productCategory.length) {
          object.productCategory = [];
          for (var j = 0; j < message.productCategory.length; ++j)
            object.productCategory[j] =
              $root.Product.Request.Body.Productcategory.toObject(
                message.productCategory[j],
                options,
              );
        }
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          object.createdBy = message.createdBy;
        return object;
      };

      /**
       * Converts this Body to JSON.
       * @function toJSON
       * @memberof Product.Request.Body
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Body.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      Body.Productimage = (function () {
        /**
         * Properties of a Productimage.
         * @memberof Product.Request.Body
         * @interface IProductimage
         * @property {number|null} [id] Productimage id
         * @property {number|null} [productId] Productimage productId
         * @property {string|null} [nameImage] Productimage nameImage
         * @property {string|null} [altImage] Productimage altImage
         * @property {string|null} [createdDate] Productimage createdDate
         * @property {string|null} [updatedDate] Productimage updatedDate
         */

        /**
         * Constructs a new Productimage.
         * @memberof Product.Request.Body
         * @classdesc Represents a Productimage.
         * @implements IProductimage
         * @constructor
         * @param {Product.Request.Body.IProductimage=} [properties] Properties to set
         */
        function Productimage(properties) {
          if (properties)
            for (
              var keys = Object.keys(properties), i = 0;
              i < keys.length;
              ++i
            )
              if (properties[keys[i]] != null)
                this[keys[i]] = properties[keys[i]];
        }

        /**
         * Productimage id.
         * @member {number} id
         * @memberof Product.Request.Body.Productimage
         * @instance
         */
        Productimage.prototype.id = 0;

        /**
         * Productimage productId.
         * @member {number} productId
         * @memberof Product.Request.Body.Productimage
         * @instance
         */
        Productimage.prototype.productId = 0;

        /**
         * Productimage nameImage.
         * @member {string} nameImage
         * @memberof Product.Request.Body.Productimage
         * @instance
         */
        Productimage.prototype.nameImage = '';

        /**
         * Productimage altImage.
         * @member {string} altImage
         * @memberof Product.Request.Body.Productimage
         * @instance
         */
        Productimage.prototype.altImage = '';

        /**
         * Productimage createdDate.
         * @member {string} createdDate
         * @memberof Product.Request.Body.Productimage
         * @instance
         */
        Productimage.prototype.createdDate = '';

        /**
         * Productimage updatedDate.
         * @member {string} updatedDate
         * @memberof Product.Request.Body.Productimage
         * @instance
         */
        Productimage.prototype.updatedDate = '';

        /**
         * Creates a new Productimage instance using the specified properties.
         * @function create
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {Product.Request.Body.IProductimage=} [properties] Properties to set
         * @returns {Product.Request.Body.Productimage} Productimage instance
         */
        Productimage.create = function create(properties) {
          return new Productimage(properties);
        };

        /**
         * Encodes the specified Productimage message. Does not implicitly {@link Product.Request.Body.Productimage.verify|verify} messages.
         * @function encode
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {Product.Request.Body.IProductimage} message Productimage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productimage.encode = function encode(message, writer) {
          if (!writer) writer = $Writer.create();
          if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
            writer.uint32(/* id 1, wireType 0 =*/ 8).uint32(message.id);
          if (
            message.productId != null &&
            Object.hasOwnProperty.call(message, 'productId')
          )
            writer.uint32(/* id 2, wireType 0 =*/ 16).uint32(message.productId);
          if (
            message.nameImage != null &&
            Object.hasOwnProperty.call(message, 'nameImage')
          )
            writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.nameImage);
          if (
            message.altImage != null &&
            Object.hasOwnProperty.call(message, 'altImage')
          )
            writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.altImage);
          if (
            message.createdDate != null &&
            Object.hasOwnProperty.call(message, 'createdDate')
          )
            writer
              .uint32(/* id 5, wireType 2 =*/ 42)
              .string(message.createdDate);
          if (
            message.updatedDate != null &&
            Object.hasOwnProperty.call(message, 'updatedDate')
          )
            writer
              .uint32(/* id 6, wireType 2 =*/ 50)
              .string(message.updatedDate);
          return writer;
        };

        /**
         * Encodes the specified Productimage message, length delimited. Does not implicitly {@link Product.Request.Body.Productimage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {Product.Request.Body.IProductimage} message Productimage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productimage.encodeDelimited = function encodeDelimited(
          message,
          writer,
        ) {
          return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Productimage message from the specified reader or buffer.
         * @function decode
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Product.Request.Body.Productimage} Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productimage.decode = function decode(reader, length) {
          if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
          var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.Product.Request.Body.Productimage();
          while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
              case 1:
                message.id = reader.uint32();
                break;
              case 2:
                message.productId = reader.uint32();
                break;
              case 3:
                message.nameImage = reader.string();
                break;
              case 4:
                message.altImage = reader.string();
                break;
              case 5:
                message.createdDate = reader.string();
                break;
              case 6:
                message.updatedDate = reader.string();
                break;
              default:
                reader.skipType(tag & 7);
                break;
            }
          }
          return message;
        };

        /**
         * Decodes a Productimage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Product.Request.Body.Productimage} Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productimage.decodeDelimited = function decodeDelimited(reader) {
          if (!(reader instanceof $Reader)) reader = new $Reader(reader);
          return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Productimage message.
         * @function verify
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Productimage.verify = function verify(message) {
          if (typeof message !== 'object' || message === null)
            return 'object expected';
          if (message.id != null && message.hasOwnProperty('id'))
            if (!$util.isInteger(message.id)) return 'id: integer expected';
          if (message.productId != null && message.hasOwnProperty('productId'))
            if (!$util.isInteger(message.productId))
              return 'productId: integer expected';
          if (message.nameImage != null && message.hasOwnProperty('nameImage'))
            if (!$util.isString(message.nameImage))
              return 'nameImage: string expected';
          if (message.altImage != null && message.hasOwnProperty('altImage'))
            if (!$util.isString(message.altImage))
              return 'altImage: string expected';
          if (
            message.createdDate != null &&
            message.hasOwnProperty('createdDate')
          )
            if (!$util.isString(message.createdDate))
              return 'createdDate: string expected';
          if (
            message.updatedDate != null &&
            message.hasOwnProperty('updatedDate')
          )
            if (!$util.isString(message.updatedDate))
              return 'updatedDate: string expected';
          return null;
        };

        /**
         * Creates a Productimage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Product.Request.Body.Productimage} Productimage
         */
        Productimage.fromObject = function fromObject(object) {
          if (object instanceof $root.Product.Request.Body.Productimage)
            return object;
          var message = new $root.Product.Request.Body.Productimage();
          if (object.id != null) message.id = object.id >>> 0;
          if (object.productId != null)
            message.productId = object.productId >>> 0;
          if (object.nameImage != null)
            message.nameImage = String(object.nameImage);
          if (object.altImage != null)
            message.altImage = String(object.altImage);
          if (object.createdDate != null)
            message.createdDate = String(object.createdDate);
          if (object.updatedDate != null)
            message.updatedDate = String(object.updatedDate);
          return message;
        };

        /**
         * Creates a plain object from a Productimage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Product.Request.Body.Productimage
         * @static
         * @param {Product.Request.Body.Productimage} message Productimage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Productimage.toObject = function toObject(message, options) {
          if (!options) options = {};
          var object = {};
          if (options.defaults) {
            object.id = 0;
            object.productId = 0;
            object.nameImage = '';
            object.altImage = '';
            object.createdDate = '';
            object.updatedDate = '';
          }
          if (message.id != null && message.hasOwnProperty('id'))
            object.id = message.id;
          if (message.productId != null && message.hasOwnProperty('productId'))
            object.productId = message.productId;
          if (message.nameImage != null && message.hasOwnProperty('nameImage'))
            object.nameImage = message.nameImage;
          if (message.altImage != null && message.hasOwnProperty('altImage'))
            object.altImage = message.altImage;
          if (
            message.createdDate != null &&
            message.hasOwnProperty('createdDate')
          )
            object.createdDate = message.createdDate;
          if (
            message.updatedDate != null &&
            message.hasOwnProperty('updatedDate')
          )
            object.updatedDate = message.updatedDate;
          return object;
        };

        /**
         * Converts this Productimage to JSON.
         * @function toJSON
         * @memberof Product.Request.Body.Productimage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Productimage.prototype.toJSON = function toJSON() {
          return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Productimage;
      })();

      Body.Productcategory = (function () {
        /**
         * Properties of a Productcategory.
         * @memberof Product.Request.Body
         * @interface IProductcategory
         * @property {string|null} [categoryName] Productcategory categoryName
         * @property {string|null} [categorySlug] Productcategory categorySlug
         */

        /**
         * Constructs a new Productcategory.
         * @memberof Product.Request.Body
         * @classdesc Represents a Productcategory.
         * @implements IProductcategory
         * @constructor
         * @param {Product.Request.Body.IProductcategory=} [properties] Properties to set
         */
        function Productcategory(properties) {
          if (properties)
            for (
              var keys = Object.keys(properties), i = 0;
              i < keys.length;
              ++i
            )
              if (properties[keys[i]] != null)
                this[keys[i]] = properties[keys[i]];
        }

        /**
         * Productcategory categoryName.
         * @member {string} categoryName
         * @memberof Product.Request.Body.Productcategory
         * @instance
         */
        Productcategory.prototype.categoryName = '';

        /**
         * Productcategory categorySlug.
         * @member {string} categorySlug
         * @memberof Product.Request.Body.Productcategory
         * @instance
         */
        Productcategory.prototype.categorySlug = '';

        /**
         * Creates a new Productcategory instance using the specified properties.
         * @function create
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {Product.Request.Body.IProductcategory=} [properties] Properties to set
         * @returns {Product.Request.Body.Productcategory} Productcategory instance
         */
        Productcategory.create = function create(properties) {
          return new Productcategory(properties);
        };

        /**
         * Encodes the specified Productcategory message. Does not implicitly {@link Product.Request.Body.Productcategory.verify|verify} messages.
         * @function encode
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {Product.Request.Body.IProductcategory} message Productcategory message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productcategory.encode = function encode(message, writer) {
          if (!writer) writer = $Writer.create();
          if (
            message.categoryName != null &&
            Object.hasOwnProperty.call(message, 'categoryName')
          )
            writer
              .uint32(/* id 1, wireType 2 =*/ 10)
              .string(message.categoryName);
          if (
            message.categorySlug != null &&
            Object.hasOwnProperty.call(message, 'categorySlug')
          )
            writer
              .uint32(/* id 2, wireType 2 =*/ 18)
              .string(message.categorySlug);
          return writer;
        };

        /**
         * Encodes the specified Productcategory message, length delimited. Does not implicitly {@link Product.Request.Body.Productcategory.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {Product.Request.Body.IProductcategory} message Productcategory message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productcategory.encodeDelimited = function encodeDelimited(
          message,
          writer,
        ) {
          return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Productcategory message from the specified reader or buffer.
         * @function decode
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Product.Request.Body.Productcategory} Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productcategory.decode = function decode(reader, length) {
          if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
          var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.Product.Request.Body.Productcategory();
          while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
              case 1:
                message.categoryName = reader.string();
                break;
              case 2:
                message.categorySlug = reader.string();
                break;
              default:
                reader.skipType(tag & 7);
                break;
            }
          }
          return message;
        };

        /**
         * Decodes a Productcategory message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Product.Request.Body.Productcategory} Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productcategory.decodeDelimited = function decodeDelimited(reader) {
          if (!(reader instanceof $Reader)) reader = new $Reader(reader);
          return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Productcategory message.
         * @function verify
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Productcategory.verify = function verify(message) {
          if (typeof message !== 'object' || message === null)
            return 'object expected';
          if (
            message.categoryName != null &&
            message.hasOwnProperty('categoryName')
          )
            if (!$util.isString(message.categoryName))
              return 'categoryName: string expected';
          if (
            message.categorySlug != null &&
            message.hasOwnProperty('categorySlug')
          )
            if (!$util.isString(message.categorySlug))
              return 'categorySlug: string expected';
          return null;
        };

        /**
         * Creates a Productcategory message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Product.Request.Body.Productcategory} Productcategory
         */
        Productcategory.fromObject = function fromObject(object) {
          if (object instanceof $root.Product.Request.Body.Productcategory)
            return object;
          var message = new $root.Product.Request.Body.Productcategory();
          if (object.categoryName != null)
            message.categoryName = String(object.categoryName);
          if (object.categorySlug != null)
            message.categorySlug = String(object.categorySlug);
          return message;
        };

        /**
         * Creates a plain object from a Productcategory message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Product.Request.Body.Productcategory
         * @static
         * @param {Product.Request.Body.Productcategory} message Productcategory
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Productcategory.toObject = function toObject(message, options) {
          if (!options) options = {};
          var object = {};
          if (options.defaults) {
            object.categoryName = '';
            object.categorySlug = '';
          }
          if (
            message.categoryName != null &&
            message.hasOwnProperty('categoryName')
          )
            object.categoryName = message.categoryName;
          if (
            message.categorySlug != null &&
            message.hasOwnProperty('categorySlug')
          )
            object.categorySlug = message.categorySlug;
          return object;
        };

        /**
         * Converts this Productcategory to JSON.
         * @function toJSON
         * @memberof Product.Request.Body.Productcategory
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Productcategory.prototype.toJSON = function toJSON() {
          return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Productcategory;
      })();

      return Body;
    })();

    Request.Params = (function () {
      /**
       * Properties of a Params.
       * @memberof Product.Request
       * @interface IParams
       * @property {string|null} [id] Params id
       * @property {Array.<number>|null} [ids] Params ids
       * @property {string|null} [flag] Params flag
       */

      /**
       * Constructs a new Params.
       * @memberof Product.Request
       * @classdesc Represents a Params.
       * @implements IParams
       * @constructor
       * @param {Product.Request.IParams=} [properties] Properties to set
       */
      function Params(properties) {
        this.ids = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Params id.
       * @member {string} id
       * @memberof Product.Request.Params
       * @instance
       */
      Params.prototype.id = '';

      /**
       * Params ids.
       * @member {Array.<number>} ids
       * @memberof Product.Request.Params
       * @instance
       */
      Params.prototype.ids = $util.emptyArray;

      /**
       * Params flag.
       * @member {string|null|undefined} flag
       * @memberof Product.Request.Params
       * @instance
       */
      Params.prototype.flag = null;

      // OneOf field names bound to virtual getters and setters
      var $oneOfFields;

      /**
       * Params _flag.
       * @member {"flag"|undefined} _flag
       * @memberof Product.Request.Params
       * @instance
       */
      Object.defineProperty(Params.prototype, '_flag', {
        get: $util.oneOfGetter(($oneOfFields = ['flag'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Creates a new Params instance using the specified properties.
       * @function create
       * @memberof Product.Request.Params
       * @static
       * @param {Product.Request.IParams=} [properties] Properties to set
       * @returns {Product.Request.Params} Params instance
       */
      Params.create = function create(properties) {
        return new Params(properties);
      };

      /**
       * Encodes the specified Params message. Does not implicitly {@link Product.Request.Params.verify|verify} messages.
       * @function encode
       * @memberof Product.Request.Params
       * @static
       * @param {Product.Request.IParams} message Params message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Params.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.id);
        if (message.ids != null && message.ids.length) {
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork();
          for (var i = 0; i < message.ids.length; ++i)
            writer.int32(message.ids[i]);
          writer.ldelim();
        }
        if (message.flag != null && Object.hasOwnProperty.call(message, 'flag'))
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.flag);
        return writer;
      };

      /**
       * Encodes the specified Params message, length delimited. Does not implicitly {@link Product.Request.Params.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.Request.Params
       * @static
       * @param {Product.Request.IParams} message Params message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Params.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Params message from the specified reader or buffer.
       * @function decode
       * @memberof Product.Request.Params
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.Request.Params} Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Params.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.Request.Params();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.string();
              break;
            case 2:
              if (!(message.ids && message.ids.length)) message.ids = [];
              if ((tag & 7) === 2) {
                var end2 = reader.uint32() + reader.pos;
                while (reader.pos < end2) message.ids.push(reader.int32());
              } else message.ids.push(reader.int32());
              break;
            case 3:
              message.flag = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Params message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.Request.Params
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.Request.Params} Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Params.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Params message.
       * @function verify
       * @memberof Product.Request.Params
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Params.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        var properties = {};
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isString(message.id)) return 'id: string expected';
        if (message.ids != null && message.hasOwnProperty('ids')) {
          if (!Array.isArray(message.ids)) return 'ids: array expected';
          for (var i = 0; i < message.ids.length; ++i)
            if (!$util.isInteger(message.ids[i]))
              return 'ids: integer[] expected';
        }
        if (message.flag != null && message.hasOwnProperty('flag')) {
          properties._flag = 1;
          if (!$util.isString(message.flag)) return 'flag: string expected';
        }
        return null;
      };

      /**
       * Creates a Params message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.Request.Params
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.Request.Params} Params
       */
      Params.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.Request.Params) return object;
        var message = new $root.Product.Request.Params();
        if (object.id != null) message.id = String(object.id);
        if (object.ids) {
          if (!Array.isArray(object.ids))
            throw TypeError('.Product.Request.Params.ids: array expected');
          message.ids = [];
          for (var i = 0; i < object.ids.length; ++i)
            message.ids[i] = object.ids[i] | 0;
        }
        if (object.flag != null) message.flag = String(object.flag);
        return message;
      };

      /**
       * Creates a plain object from a Params message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.Request.Params
       * @static
       * @param {Product.Request.Params} message Params
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Params.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.ids = [];
        if (options.defaults) object.id = '';
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        if (message.ids && message.ids.length) {
          object.ids = [];
          for (var j = 0; j < message.ids.length; ++j)
            object.ids[j] = message.ids[j];
        }
        if (message.flag != null && message.hasOwnProperty('flag')) {
          object.flag = message.flag;
          if (options.oneofs) object._flag = 'flag';
        }
        return object;
      };

      /**
       * Converts this Params to JSON.
       * @function toJSON
       * @memberof Product.Request.Params
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Params.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Params;
    })();

    Request.User = (function () {
      /**
       * Properties of a User.
       * @memberof Product.Request
       * @interface IUser
       * @property {number|null} [id] User id
       */

      /**
       * Constructs a new User.
       * @memberof Product.Request
       * @classdesc Represents a User.
       * @implements IUser
       * @constructor
       * @param {Product.Request.IUser=} [properties] Properties to set
       */
      function User(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * User id.
       * @member {number} id
       * @memberof Product.Request.User
       * @instance
       */
      User.prototype.id = 0;

      /**
       * Creates a new User instance using the specified properties.
       * @function create
       * @memberof Product.Request.User
       * @static
       * @param {Product.Request.IUser=} [properties] Properties to set
       * @returns {Product.Request.User} User instance
       */
      User.create = function create(properties) {
        return new User(properties);
      };

      /**
       * Encodes the specified User message. Does not implicitly {@link Product.Request.User.verify|verify} messages.
       * @function encode
       * @memberof Product.Request.User
       * @static
       * @param {Product.Request.IUser} message User message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      User.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.id);
        return writer;
      };

      /**
       * Encodes the specified User message, length delimited. Does not implicitly {@link Product.Request.User.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.Request.User
       * @static
       * @param {Product.Request.IUser} message User message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      User.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a User message from the specified reader or buffer.
       * @function decode
       * @memberof Product.Request.User
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.Request.User} User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      User.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.Request.User();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.int32();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a User message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.Request.User
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.Request.User} User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      User.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a User message.
       * @function verify
       * @memberof Product.Request.User
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      User.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isInteger(message.id)) return 'id: integer expected';
        return null;
      };

      /**
       * Creates a User message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.Request.User
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.Request.User} User
       */
      User.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.Request.User) return object;
        var message = new $root.Product.Request.User();
        if (object.id != null) message.id = object.id | 0;
        return message;
      };

      /**
       * Creates a plain object from a User message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.Request.User
       * @static
       * @param {Product.Request.User} message User
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      User.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) object.id = 0;
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        return object;
      };

      /**
       * Converts this User to JSON.
       * @function toJSON
       * @memberof Product.Request.User
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      User.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return User;
    })();

    return Request;
  })();

  Product.ResponseGetAll = (function () {
    /**
     * Properties of a ResponseGetAll.
     * @memberof Product
     * @interface IResponseGetAll
     * @property {Array.<Product.ResponseGetAll.IData>|null} [data] ResponseGetAll data
     * @property {Product.ResponseGetAll.IMeta|null} [meta] ResponseGetAll meta
     * @property {boolean|null} [success] ResponseGetAll success
     * @property {string|null} [message] ResponseGetAll message
     */

    /**
     * Constructs a new ResponseGetAll.
     * @memberof Product
     * @classdesc Represents a ResponseGetAll.
     * @implements IResponseGetAll
     * @constructor
     * @param {Product.IResponseGetAll=} [properties] Properties to set
     */
    function ResponseGetAll(properties) {
      this.data = [];
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseGetAll data.
     * @member {Array.<Product.ResponseGetAll.IData>} data
     * @memberof Product.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.data = $util.emptyArray;

    /**
     * ResponseGetAll meta.
     * @member {Product.ResponseGetAll.IMeta|null|undefined} meta
     * @memberof Product.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.meta = null;

    /**
     * ResponseGetAll success.
     * @member {boolean} success
     * @memberof Product.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.success = false;

    /**
     * ResponseGetAll message.
     * @member {string} message
     * @memberof Product.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.message = '';

    /**
     * Creates a new ResponseGetAll instance using the specified properties.
     * @function create
     * @memberof Product.ResponseGetAll
     * @static
     * @param {Product.IResponseGetAll=} [properties] Properties to set
     * @returns {Product.ResponseGetAll} ResponseGetAll instance
     */
    ResponseGetAll.create = function create(properties) {
      return new ResponseGetAll(properties);
    };

    /**
     * Encodes the specified ResponseGetAll message. Does not implicitly {@link Product.ResponseGetAll.verify|verify} messages.
     * @function encode
     * @memberof Product.ResponseGetAll
     * @static
     * @param {Product.IResponseGetAll} message ResponseGetAll message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseGetAll.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && message.data.length)
        for (var i = 0; i < message.data.length; ++i)
          $root.Product.ResponseGetAll.Data.encode(
            message.data[i],
            writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
          ).ldelim();
      if (message.meta != null && Object.hasOwnProperty.call(message, 'meta'))
        $root.Product.ResponseGetAll.Meta.encode(
          message.meta,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 3, wireType 0 =*/ 24).bool(message.success);
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified ResponseGetAll message, length delimited. Does not implicitly {@link Product.ResponseGetAll.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Product.ResponseGetAll
     * @static
     * @param {Product.IResponseGetAll} message ResponseGetAll message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseGetAll.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer.
     * @function decode
     * @memberof Product.ResponseGetAll
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Product.ResponseGetAll} ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseGetAll.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Product.ResponseGetAll();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            if (!(message.data && message.data.length)) message.data = [];
            message.data.push(
              $root.Product.ResponseGetAll.Data.decode(reader, reader.uint32()),
            );
            break;
          case 2:
            message.meta = $root.Product.ResponseGetAll.Meta.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.success = reader.bool();
            break;
          case 4:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Product.ResponseGetAll
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Product.ResponseGetAll} ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseGetAll.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseGetAll message.
     * @function verify
     * @memberof Product.ResponseGetAll
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseGetAll.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        if (!Array.isArray(message.data)) return 'data: array expected';
        for (var i = 0; i < message.data.length; ++i) {
          var error = $root.Product.ResponseGetAll.Data.verify(message.data[i]);
          if (error) return 'data.' + error;
        }
      }
      if (message.meta != null && message.hasOwnProperty('meta')) {
        var error = $root.Product.ResponseGetAll.Meta.verify(message.meta);
        if (error) return 'meta.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseGetAll message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Product.ResponseGetAll
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Product.ResponseGetAll} ResponseGetAll
     */
    ResponseGetAll.fromObject = function fromObject(object) {
      if (object instanceof $root.Product.ResponseGetAll) return object;
      var message = new $root.Product.ResponseGetAll();
      if (object.data) {
        if (!Array.isArray(object.data))
          throw TypeError('.Product.ResponseGetAll.data: array expected');
        message.data = [];
        for (var i = 0; i < object.data.length; ++i) {
          if (typeof object.data[i] !== 'object')
            throw TypeError('.Product.ResponseGetAll.data: object expected');
          message.data[i] = $root.Product.ResponseGetAll.Data.fromObject(
            object.data[i],
          );
        }
      }
      if (object.meta != null) {
        if (typeof object.meta !== 'object')
          throw TypeError('.Product.ResponseGetAll.meta: object expected');
        message.meta = $root.Product.ResponseGetAll.Meta.fromObject(
          object.meta,
        );
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseGetAll message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Product.ResponseGetAll
     * @static
     * @param {Product.ResponseGetAll} message ResponseGetAll
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseGetAll.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.arrays || options.defaults) object.data = [];
      if (options.defaults) {
        object.meta = null;
        object.success = false;
        object.message = '';
      }
      if (message.data && message.data.length) {
        object.data = [];
        for (var j = 0; j < message.data.length; ++j)
          object.data[j] = $root.Product.ResponseGetAll.Data.toObject(
            message.data[j],
            options,
          );
      }
      if (message.meta != null && message.hasOwnProperty('meta'))
        object.meta = $root.Product.ResponseGetAll.Meta.toObject(
          message.meta,
          options,
        );
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      return object;
    };

    /**
     * Converts this ResponseGetAll to JSON.
     * @function toJSON
     * @memberof Product.ResponseGetAll
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseGetAll.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    ResponseGetAll.Data = (function () {
      /**
       * Properties of a Data.
       * @memberof Product.ResponseGetAll
       * @interface IData
       * @property {string|null} [id] Data id
       * @property {string|null} [productName] Data productName
       * @property {string|null} [productLogo] Data productLogo
       * @property {string|null} [productDescription] Data productDescription
       * @property {string|null} [productShortDescription] Data productShortDescription
       * @property {string|null} [metaTitleProduct] Data metaTitleProduct
       * @property {string|null} [metaDescriptionProduct] Data metaDescriptionProduct
       * @property {string|null} [productSlug] Data productSlug
       * @property {string|null} [metaKeyword] Data metaKeyword
       * @property {number|null} [price] Data price
       * @property {Array.<Product.ResponseGetAll.Data.IProductimage>|null} [productImage] Data productImage
       * @property {Array.<Product.ResponseGetAll.Data.IProductcategory>|null} [productCategory] Data productCategory
       * @property {number|null} [createdBy] Data createdBy
       */

      /**
       * Constructs a new Data.
       * @memberof Product.ResponseGetAll
       * @classdesc Represents a Data.
       * @implements IData
       * @constructor
       * @param {Product.ResponseGetAll.IData=} [properties] Properties to set
       */
      function Data(properties) {
        this.productImage = [];
        this.productCategory = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Data id.
       * @member {string} id
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.id = '';

      /**
       * Data productName.
       * @member {string} productName
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productName = '';

      /**
       * Data productLogo.
       * @member {string} productLogo
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productLogo = '';

      /**
       * Data productDescription.
       * @member {string} productDescription
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productDescription = '';

      /**
       * Data productShortDescription.
       * @member {string} productShortDescription
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productShortDescription = '';

      /**
       * Data metaTitleProduct.
       * @member {string} metaTitleProduct
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.metaTitleProduct = '';

      /**
       * Data metaDescriptionProduct.
       * @member {string} metaDescriptionProduct
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.metaDescriptionProduct = '';

      /**
       * Data productSlug.
       * @member {string} productSlug
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productSlug = '';

      /**
       * Data metaKeyword.
       * @member {string} metaKeyword
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.metaKeyword = '';

      /**
       * Data price.
       * @member {number} price
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.price = 0;

      /**
       * Data productImage.
       * @member {Array.<Product.ResponseGetAll.Data.IProductimage>} productImage
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productImage = $util.emptyArray;

      /**
       * Data productCategory.
       * @member {Array.<Product.ResponseGetAll.Data.IProductcategory>} productCategory
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.productCategory = $util.emptyArray;

      /**
       * Data createdBy.
       * @member {number} createdBy
       * @memberof Product.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.createdBy = 0;

      /**
       * Creates a new Data instance using the specified properties.
       * @function create
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {Product.ResponseGetAll.IData=} [properties] Properties to set
       * @returns {Product.ResponseGetAll.Data} Data instance
       */
      Data.create = function create(properties) {
        return new Data(properties);
      };

      /**
       * Encodes the specified Data message. Does not implicitly {@link Product.ResponseGetAll.Data.verify|verify} messages.
       * @function encode
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {Product.ResponseGetAll.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.id);
        if (
          message.productName != null &&
          Object.hasOwnProperty.call(message, 'productName')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.productName);
        if (
          message.productLogo != null &&
          Object.hasOwnProperty.call(message, 'productLogo')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.productLogo);
        if (
          message.productDescription != null &&
          Object.hasOwnProperty.call(message, 'productDescription')
        )
          writer
            .uint32(/* id 4, wireType 2 =*/ 34)
            .string(message.productDescription);
        if (
          message.productShortDescription != null &&
          Object.hasOwnProperty.call(message, 'productShortDescription')
        )
          writer
            .uint32(/* id 5, wireType 2 =*/ 42)
            .string(message.productShortDescription);
        if (
          message.metaTitleProduct != null &&
          Object.hasOwnProperty.call(message, 'metaTitleProduct')
        )
          writer
            .uint32(/* id 6, wireType 2 =*/ 50)
            .string(message.metaTitleProduct);
        if (
          message.metaDescriptionProduct != null &&
          Object.hasOwnProperty.call(message, 'metaDescriptionProduct')
        )
          writer
            .uint32(/* id 7, wireType 2 =*/ 58)
            .string(message.metaDescriptionProduct);
        if (
          message.productSlug != null &&
          Object.hasOwnProperty.call(message, 'productSlug')
        )
          writer.uint32(/* id 8, wireType 2 =*/ 66).string(message.productSlug);
        if (
          message.metaKeyword != null &&
          Object.hasOwnProperty.call(message, 'metaKeyword')
        )
          writer.uint32(/* id 9, wireType 2 =*/ 74).string(message.metaKeyword);
        if (
          message.price != null &&
          Object.hasOwnProperty.call(message, 'price')
        )
          writer.uint32(/* id 10, wireType 0 =*/ 80).uint32(message.price);
        if (message.productImage != null && message.productImage.length)
          for (var i = 0; i < message.productImage.length; ++i)
            $root.Product.ResponseGetAll.Data.Productimage.encode(
              message.productImage[i],
              writer.uint32(/* id 11, wireType 2 =*/ 90).fork(),
            ).ldelim();
        if (message.productCategory != null && message.productCategory.length)
          for (var i = 0; i < message.productCategory.length; ++i)
            $root.Product.ResponseGetAll.Data.Productcategory.encode(
              message.productCategory[i],
              writer.uint32(/* id 12, wireType 2 =*/ 98).fork(),
            ).ldelim();
        if (
          message.createdBy != null &&
          Object.hasOwnProperty.call(message, 'createdBy')
        )
          writer.uint32(/* id 13, wireType 0 =*/ 104).uint32(message.createdBy);
        return writer;
      };

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Data.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {Product.ResponseGetAll.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @function decode
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.ResponseGetAll.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.ResponseGetAll.Data();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.string();
              break;
            case 2:
              message.productName = reader.string();
              break;
            case 3:
              message.productLogo = reader.string();
              break;
            case 4:
              message.productDescription = reader.string();
              break;
            case 5:
              message.productShortDescription = reader.string();
              break;
            case 6:
              message.metaTitleProduct = reader.string();
              break;
            case 7:
              message.metaDescriptionProduct = reader.string();
              break;
            case 8:
              message.productSlug = reader.string();
              break;
            case 9:
              message.metaKeyword = reader.string();
              break;
            case 10:
              message.price = reader.uint32();
              break;
            case 11:
              if (!(message.productImage && message.productImage.length))
                message.productImage = [];
              message.productImage.push(
                $root.Product.ResponseGetAll.Data.Productimage.decode(
                  reader,
                  reader.uint32(),
                ),
              );
              break;
            case 12:
              if (!(message.productCategory && message.productCategory.length))
                message.productCategory = [];
              message.productCategory.push(
                $root.Product.ResponseGetAll.Data.Productcategory.decode(
                  reader,
                  reader.uint32(),
                ),
              );
              break;
            case 13:
              message.createdBy = reader.uint32();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.ResponseGetAll.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Data message.
       * @function verify
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Data.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isString(message.id)) return 'id: string expected';
        if (
          message.productName != null &&
          message.hasOwnProperty('productName')
        )
          if (!$util.isString(message.productName))
            return 'productName: string expected';
        if (
          message.productLogo != null &&
          message.hasOwnProperty('productLogo')
        )
          if (!$util.isString(message.productLogo))
            return 'productLogo: string expected';
        if (
          message.productDescription != null &&
          message.hasOwnProperty('productDescription')
        )
          if (!$util.isString(message.productDescription))
            return 'productDescription: string expected';
        if (
          message.productShortDescription != null &&
          message.hasOwnProperty('productShortDescription')
        )
          if (!$util.isString(message.productShortDescription))
            return 'productShortDescription: string expected';
        if (
          message.metaTitleProduct != null &&
          message.hasOwnProperty('metaTitleProduct')
        )
          if (!$util.isString(message.metaTitleProduct))
            return 'metaTitleProduct: string expected';
        if (
          message.metaDescriptionProduct != null &&
          message.hasOwnProperty('metaDescriptionProduct')
        )
          if (!$util.isString(message.metaDescriptionProduct))
            return 'metaDescriptionProduct: string expected';
        if (
          message.productSlug != null &&
          message.hasOwnProperty('productSlug')
        )
          if (!$util.isString(message.productSlug))
            return 'productSlug: string expected';
        if (
          message.metaKeyword != null &&
          message.hasOwnProperty('metaKeyword')
        )
          if (!$util.isString(message.metaKeyword))
            return 'metaKeyword: string expected';
        if (message.price != null && message.hasOwnProperty('price'))
          if (!$util.isInteger(message.price)) return 'price: integer expected';
        if (
          message.productImage != null &&
          message.hasOwnProperty('productImage')
        ) {
          if (!Array.isArray(message.productImage))
            return 'productImage: array expected';
          for (var i = 0; i < message.productImage.length; ++i) {
            var error = $root.Product.ResponseGetAll.Data.Productimage.verify(
              message.productImage[i],
            );
            if (error) return 'productImage.' + error;
          }
        }
        if (
          message.productCategory != null &&
          message.hasOwnProperty('productCategory')
        ) {
          if (!Array.isArray(message.productCategory))
            return 'productCategory: array expected';
          for (var i = 0; i < message.productCategory.length; ++i) {
            var error =
              $root.Product.ResponseGetAll.Data.Productcategory.verify(
                message.productCategory[i],
              );
            if (error) return 'productCategory.' + error;
          }
        }
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          if (!$util.isInteger(message.createdBy))
            return 'createdBy: integer expected';
        return null;
      };

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.ResponseGetAll.Data} Data
       */
      Data.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.ResponseGetAll.Data) return object;
        var message = new $root.Product.ResponseGetAll.Data();
        if (object.id != null) message.id = String(object.id);
        if (object.productName != null)
          message.productName = String(object.productName);
        if (object.productLogo != null)
          message.productLogo = String(object.productLogo);
        if (object.productDescription != null)
          message.productDescription = String(object.productDescription);
        if (object.productShortDescription != null)
          message.productShortDescription = String(
            object.productShortDescription,
          );
        if (object.metaTitleProduct != null)
          message.metaTitleProduct = String(object.metaTitleProduct);
        if (object.metaDescriptionProduct != null)
          message.metaDescriptionProduct = String(
            object.metaDescriptionProduct,
          );
        if (object.productSlug != null)
          message.productSlug = String(object.productSlug);
        if (object.metaKeyword != null)
          message.metaKeyword = String(object.metaKeyword);
        if (object.price != null) message.price = object.price >>> 0;
        if (object.productImage) {
          if (!Array.isArray(object.productImage))
            throw TypeError(
              '.Product.ResponseGetAll.Data.productImage: array expected',
            );
          message.productImage = [];
          for (var i = 0; i < object.productImage.length; ++i) {
            if (typeof object.productImage[i] !== 'object')
              throw TypeError(
                '.Product.ResponseGetAll.Data.productImage: object expected',
              );
            message.productImage[i] =
              $root.Product.ResponseGetAll.Data.Productimage.fromObject(
                object.productImage[i],
              );
          }
        }
        if (object.productCategory) {
          if (!Array.isArray(object.productCategory))
            throw TypeError(
              '.Product.ResponseGetAll.Data.productCategory: array expected',
            );
          message.productCategory = [];
          for (var i = 0; i < object.productCategory.length; ++i) {
            if (typeof object.productCategory[i] !== 'object')
              throw TypeError(
                '.Product.ResponseGetAll.Data.productCategory: object expected',
              );
            message.productCategory[i] =
              $root.Product.ResponseGetAll.Data.Productcategory.fromObject(
                object.productCategory[i],
              );
          }
        }
        if (object.createdBy != null)
          message.createdBy = object.createdBy >>> 0;
        return message;
      };

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.ResponseGetAll.Data
       * @static
       * @param {Product.ResponseGetAll.Data} message Data
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Data.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) {
          object.productImage = [];
          object.productCategory = [];
        }
        if (options.defaults) {
          object.id = '';
          object.productName = '';
          object.productLogo = '';
          object.productDescription = '';
          object.productShortDescription = '';
          object.metaTitleProduct = '';
          object.metaDescriptionProduct = '';
          object.productSlug = '';
          object.metaKeyword = '';
          object.price = 0;
          object.createdBy = 0;
        }
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        if (
          message.productName != null &&
          message.hasOwnProperty('productName')
        )
          object.productName = message.productName;
        if (
          message.productLogo != null &&
          message.hasOwnProperty('productLogo')
        )
          object.productLogo = message.productLogo;
        if (
          message.productDescription != null &&
          message.hasOwnProperty('productDescription')
        )
          object.productDescription = message.productDescription;
        if (
          message.productShortDescription != null &&
          message.hasOwnProperty('productShortDescription')
        )
          object.productShortDescription = message.productShortDescription;
        if (
          message.metaTitleProduct != null &&
          message.hasOwnProperty('metaTitleProduct')
        )
          object.metaTitleProduct = message.metaTitleProduct;
        if (
          message.metaDescriptionProduct != null &&
          message.hasOwnProperty('metaDescriptionProduct')
        )
          object.metaDescriptionProduct = message.metaDescriptionProduct;
        if (
          message.productSlug != null &&
          message.hasOwnProperty('productSlug')
        )
          object.productSlug = message.productSlug;
        if (
          message.metaKeyword != null &&
          message.hasOwnProperty('metaKeyword')
        )
          object.metaKeyword = message.metaKeyword;
        if (message.price != null && message.hasOwnProperty('price'))
          object.price = message.price;
        if (message.productImage && message.productImage.length) {
          object.productImage = [];
          for (var j = 0; j < message.productImage.length; ++j)
            object.productImage[j] =
              $root.Product.ResponseGetAll.Data.Productimage.toObject(
                message.productImage[j],
                options,
              );
        }
        if (message.productCategory && message.productCategory.length) {
          object.productCategory = [];
          for (var j = 0; j < message.productCategory.length; ++j)
            object.productCategory[j] =
              $root.Product.ResponseGetAll.Data.Productcategory.toObject(
                message.productCategory[j],
                options,
              );
        }
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          object.createdBy = message.createdBy;
        return object;
      };

      /**
       * Converts this Data to JSON.
       * @function toJSON
       * @memberof Product.ResponseGetAll.Data
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Data.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      Data.Productimage = (function () {
        /**
         * Properties of a Productimage.
         * @memberof Product.ResponseGetAll.Data
         * @interface IProductimage
         * @property {string|null} [nameImage] Productimage nameImage
         * @property {string|null} [altImage] Productimage altImage
         * @property {string|null} [createdDate] Productimage createdDate
         * @property {string|null} [updatedDate] Productimage updatedDate
         */

        /**
         * Constructs a new Productimage.
         * @memberof Product.ResponseGetAll.Data
         * @classdesc Represents a Productimage.
         * @implements IProductimage
         * @constructor
         * @param {Product.ResponseGetAll.Data.IProductimage=} [properties] Properties to set
         */
        function Productimage(properties) {
          if (properties)
            for (
              var keys = Object.keys(properties), i = 0;
              i < keys.length;
              ++i
            )
              if (properties[keys[i]] != null)
                this[keys[i]] = properties[keys[i]];
        }

        /**
         * Productimage nameImage.
         * @member {string} nameImage
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @instance
         */
        Productimage.prototype.nameImage = '';

        /**
         * Productimage altImage.
         * @member {string} altImage
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @instance
         */
        Productimage.prototype.altImage = '';

        /**
         * Productimage createdDate.
         * @member {string} createdDate
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @instance
         */
        Productimage.prototype.createdDate = '';

        /**
         * Productimage updatedDate.
         * @member {string} updatedDate
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @instance
         */
        Productimage.prototype.updatedDate = '';

        /**
         * Creates a new Productimage instance using the specified properties.
         * @function create
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {Product.ResponseGetAll.Data.IProductimage=} [properties] Properties to set
         * @returns {Product.ResponseGetAll.Data.Productimage} Productimage instance
         */
        Productimage.create = function create(properties) {
          return new Productimage(properties);
        };

        /**
         * Encodes the specified Productimage message. Does not implicitly {@link Product.ResponseGetAll.Data.Productimage.verify|verify} messages.
         * @function encode
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {Product.ResponseGetAll.Data.IProductimage} message Productimage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productimage.encode = function encode(message, writer) {
          if (!writer) writer = $Writer.create();
          if (
            message.nameImage != null &&
            Object.hasOwnProperty.call(message, 'nameImage')
          )
            writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.nameImage);
          if (
            message.altImage != null &&
            Object.hasOwnProperty.call(message, 'altImage')
          )
            writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.altImage);
          if (
            message.createdDate != null &&
            Object.hasOwnProperty.call(message, 'createdDate')
          )
            writer
              .uint32(/* id 5, wireType 2 =*/ 42)
              .string(message.createdDate);
          if (
            message.updatedDate != null &&
            Object.hasOwnProperty.call(message, 'updatedDate')
          )
            writer
              .uint32(/* id 6, wireType 2 =*/ 50)
              .string(message.updatedDate);
          return writer;
        };

        /**
         * Encodes the specified Productimage message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Data.Productimage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {Product.ResponseGetAll.Data.IProductimage} message Productimage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productimage.encodeDelimited = function encodeDelimited(
          message,
          writer,
        ) {
          return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Productimage message from the specified reader or buffer.
         * @function decode
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Product.ResponseGetAll.Data.Productimage} Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productimage.decode = function decode(reader, length) {
          if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
          var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.Product.ResponseGetAll.Data.Productimage();
          while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
              case 3:
                message.nameImage = reader.string();
                break;
              case 4:
                message.altImage = reader.string();
                break;
              case 5:
                message.createdDate = reader.string();
                break;
              case 6:
                message.updatedDate = reader.string();
                break;
              default:
                reader.skipType(tag & 7);
                break;
            }
          }
          return message;
        };

        /**
         * Decodes a Productimage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Product.ResponseGetAll.Data.Productimage} Productimage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productimage.decodeDelimited = function decodeDelimited(reader) {
          if (!(reader instanceof $Reader)) reader = new $Reader(reader);
          return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Productimage message.
         * @function verify
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Productimage.verify = function verify(message) {
          if (typeof message !== 'object' || message === null)
            return 'object expected';
          if (message.nameImage != null && message.hasOwnProperty('nameImage'))
            if (!$util.isString(message.nameImage))
              return 'nameImage: string expected';
          if (message.altImage != null && message.hasOwnProperty('altImage'))
            if (!$util.isString(message.altImage))
              return 'altImage: string expected';
          if (
            message.createdDate != null &&
            message.hasOwnProperty('createdDate')
          )
            if (!$util.isString(message.createdDate))
              return 'createdDate: string expected';
          if (
            message.updatedDate != null &&
            message.hasOwnProperty('updatedDate')
          )
            if (!$util.isString(message.updatedDate))
              return 'updatedDate: string expected';
          return null;
        };

        /**
         * Creates a Productimage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Product.ResponseGetAll.Data.Productimage} Productimage
         */
        Productimage.fromObject = function fromObject(object) {
          if (object instanceof $root.Product.ResponseGetAll.Data.Productimage)
            return object;
          var message = new $root.Product.ResponseGetAll.Data.Productimage();
          if (object.nameImage != null)
            message.nameImage = String(object.nameImage);
          if (object.altImage != null)
            message.altImage = String(object.altImage);
          if (object.createdDate != null)
            message.createdDate = String(object.createdDate);
          if (object.updatedDate != null)
            message.updatedDate = String(object.updatedDate);
          return message;
        };

        /**
         * Creates a plain object from a Productimage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @static
         * @param {Product.ResponseGetAll.Data.Productimage} message Productimage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Productimage.toObject = function toObject(message, options) {
          if (!options) options = {};
          var object = {};
          if (options.defaults) {
            object.nameImage = '';
            object.altImage = '';
            object.createdDate = '';
            object.updatedDate = '';
          }
          if (message.nameImage != null && message.hasOwnProperty('nameImage'))
            object.nameImage = message.nameImage;
          if (message.altImage != null && message.hasOwnProperty('altImage'))
            object.altImage = message.altImage;
          if (
            message.createdDate != null &&
            message.hasOwnProperty('createdDate')
          )
            object.createdDate = message.createdDate;
          if (
            message.updatedDate != null &&
            message.hasOwnProperty('updatedDate')
          )
            object.updatedDate = message.updatedDate;
          return object;
        };

        /**
         * Converts this Productimage to JSON.
         * @function toJSON
         * @memberof Product.ResponseGetAll.Data.Productimage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Productimage.prototype.toJSON = function toJSON() {
          return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Productimage;
      })();

      Data.Productcategory = (function () {
        /**
         * Properties of a Productcategory.
         * @memberof Product.ResponseGetAll.Data
         * @interface IProductcategory
         * @property {string|null} [categoryName] Productcategory categoryName
         * @property {string|null} [categorySlug] Productcategory categorySlug
         */

        /**
         * Constructs a new Productcategory.
         * @memberof Product.ResponseGetAll.Data
         * @classdesc Represents a Productcategory.
         * @implements IProductcategory
         * @constructor
         * @param {Product.ResponseGetAll.Data.IProductcategory=} [properties] Properties to set
         */
        function Productcategory(properties) {
          if (properties)
            for (
              var keys = Object.keys(properties), i = 0;
              i < keys.length;
              ++i
            )
              if (properties[keys[i]] != null)
                this[keys[i]] = properties[keys[i]];
        }

        /**
         * Productcategory categoryName.
         * @member {string} categoryName
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @instance
         */
        Productcategory.prototype.categoryName = '';

        /**
         * Productcategory categorySlug.
         * @member {string} categorySlug
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @instance
         */
        Productcategory.prototype.categorySlug = '';

        /**
         * Creates a new Productcategory instance using the specified properties.
         * @function create
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {Product.ResponseGetAll.Data.IProductcategory=} [properties] Properties to set
         * @returns {Product.ResponseGetAll.Data.Productcategory} Productcategory instance
         */
        Productcategory.create = function create(properties) {
          return new Productcategory(properties);
        };

        /**
         * Encodes the specified Productcategory message. Does not implicitly {@link Product.ResponseGetAll.Data.Productcategory.verify|verify} messages.
         * @function encode
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {Product.ResponseGetAll.Data.IProductcategory} message Productcategory message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productcategory.encode = function encode(message, writer) {
          if (!writer) writer = $Writer.create();
          if (
            message.categoryName != null &&
            Object.hasOwnProperty.call(message, 'categoryName')
          )
            writer
              .uint32(/* id 1, wireType 2 =*/ 10)
              .string(message.categoryName);
          if (
            message.categorySlug != null &&
            Object.hasOwnProperty.call(message, 'categorySlug')
          )
            writer
              .uint32(/* id 2, wireType 2 =*/ 18)
              .string(message.categorySlug);
          return writer;
        };

        /**
         * Encodes the specified Productcategory message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Data.Productcategory.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {Product.ResponseGetAll.Data.IProductcategory} message Productcategory message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Productcategory.encodeDelimited = function encodeDelimited(
          message,
          writer,
        ) {
          return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Productcategory message from the specified reader or buffer.
         * @function decode
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Product.ResponseGetAll.Data.Productcategory} Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productcategory.decode = function decode(reader, length) {
          if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
          var end = length === undefined ? reader.len : reader.pos + length,
            message = new $root.Product.ResponseGetAll.Data.Productcategory();
          while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
              case 1:
                message.categoryName = reader.string();
                break;
              case 2:
                message.categorySlug = reader.string();
                break;
              default:
                reader.skipType(tag & 7);
                break;
            }
          }
          return message;
        };

        /**
         * Decodes a Productcategory message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Product.ResponseGetAll.Data.Productcategory} Productcategory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Productcategory.decodeDelimited = function decodeDelimited(reader) {
          if (!(reader instanceof $Reader)) reader = new $Reader(reader);
          return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Productcategory message.
         * @function verify
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Productcategory.verify = function verify(message) {
          if (typeof message !== 'object' || message === null)
            return 'object expected';
          if (
            message.categoryName != null &&
            message.hasOwnProperty('categoryName')
          )
            if (!$util.isString(message.categoryName))
              return 'categoryName: string expected';
          if (
            message.categorySlug != null &&
            message.hasOwnProperty('categorySlug')
          )
            if (!$util.isString(message.categorySlug))
              return 'categorySlug: string expected';
          return null;
        };

        /**
         * Creates a Productcategory message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Product.ResponseGetAll.Data.Productcategory} Productcategory
         */
        Productcategory.fromObject = function fromObject(object) {
          if (
            object instanceof $root.Product.ResponseGetAll.Data.Productcategory
          )
            return object;
          var message = new $root.Product.ResponseGetAll.Data.Productcategory();
          if (object.categoryName != null)
            message.categoryName = String(object.categoryName);
          if (object.categorySlug != null)
            message.categorySlug = String(object.categorySlug);
          return message;
        };

        /**
         * Creates a plain object from a Productcategory message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @static
         * @param {Product.ResponseGetAll.Data.Productcategory} message Productcategory
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Productcategory.toObject = function toObject(message, options) {
          if (!options) options = {};
          var object = {};
          if (options.defaults) {
            object.categoryName = '';
            object.categorySlug = '';
          }
          if (
            message.categoryName != null &&
            message.hasOwnProperty('categoryName')
          )
            object.categoryName = message.categoryName;
          if (
            message.categorySlug != null &&
            message.hasOwnProperty('categorySlug')
          )
            object.categorySlug = message.categorySlug;
          return object;
        };

        /**
         * Converts this Productcategory to JSON.
         * @function toJSON
         * @memberof Product.ResponseGetAll.Data.Productcategory
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Productcategory.prototype.toJSON = function toJSON() {
          return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Productcategory;
      })();

      return Data;
    })();

    ResponseGetAll.Meta = (function () {
      /**
       * Properties of a Meta.
       * @memberof Product.ResponseGetAll
       * @interface IMeta
       * @property {number|null} [pagination] Meta pagination
       * @property {number|null} [limit] Meta limit
       * @property {number|null} [totalPage] Meta totalPage
       * @property {number|null} [count] Meta count
       * @property {string|null} [lowerThan] Meta lowerThan
       * @property {string|null} [greaterThan] Meta greaterThan
       */

      /**
       * Constructs a new Meta.
       * @memberof Product.ResponseGetAll
       * @classdesc Represents a Meta.
       * @implements IMeta
       * @constructor
       * @param {Product.ResponseGetAll.IMeta=} [properties] Properties to set
       */
      function Meta(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Meta pagination.
       * @member {number} pagination
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.pagination = 0;

      /**
       * Meta limit.
       * @member {number} limit
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.limit = 0;

      /**
       * Meta totalPage.
       * @member {number} totalPage
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.totalPage = 0;

      /**
       * Meta count.
       * @member {number} count
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.count = 0;

      /**
       * Meta lowerThan.
       * @member {string} lowerThan
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.lowerThan = '';

      /**
       * Meta greaterThan.
       * @member {string} greaterThan
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.greaterThan = '';

      /**
       * Creates a new Meta instance using the specified properties.
       * @function create
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {Product.ResponseGetAll.IMeta=} [properties] Properties to set
       * @returns {Product.ResponseGetAll.Meta} Meta instance
       */
      Meta.create = function create(properties) {
        return new Meta(properties);
      };

      /**
       * Encodes the specified Meta message. Does not implicitly {@link Product.ResponseGetAll.Meta.verify|verify} messages.
       * @function encode
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {Product.ResponseGetAll.IMeta} message Meta message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Meta.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.pagination != null &&
          Object.hasOwnProperty.call(message, 'pagination')
        )
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.pagination);
        if (
          message.limit != null &&
          Object.hasOwnProperty.call(message, 'limit')
        )
          writer.uint32(/* id 2, wireType 0 =*/ 16).int32(message.limit);
        if (
          message.totalPage != null &&
          Object.hasOwnProperty.call(message, 'totalPage')
        )
          writer.uint32(/* id 3, wireType 0 =*/ 24).int32(message.totalPage);
        if (
          message.count != null &&
          Object.hasOwnProperty.call(message, 'count')
        )
          writer.uint32(/* id 4, wireType 0 =*/ 32).int32(message.count);
        if (
          message.lowerThan != null &&
          Object.hasOwnProperty.call(message, 'lowerThan')
        )
          writer.uint32(/* id 5, wireType 2 =*/ 42).string(message.lowerThan);
        if (
          message.greaterThan != null &&
          Object.hasOwnProperty.call(message, 'greaterThan')
        )
          writer.uint32(/* id 6, wireType 2 =*/ 50).string(message.greaterThan);
        return writer;
      };

      /**
       * Encodes the specified Meta message, length delimited. Does not implicitly {@link Product.ResponseGetAll.Meta.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {Product.ResponseGetAll.IMeta} message Meta message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Meta.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Meta message from the specified reader or buffer.
       * @function decode
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.ResponseGetAll.Meta} Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Meta.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.ResponseGetAll.Meta();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.pagination = reader.int32();
              break;
            case 2:
              message.limit = reader.int32();
              break;
            case 3:
              message.totalPage = reader.int32();
              break;
            case 4:
              message.count = reader.int32();
              break;
            case 5:
              message.lowerThan = reader.string();
              break;
            case 6:
              message.greaterThan = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Meta message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.ResponseGetAll.Meta} Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Meta.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Meta message.
       * @function verify
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Meta.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.pagination != null && message.hasOwnProperty('pagination'))
          if (!$util.isInteger(message.pagination))
            return 'pagination: integer expected';
        if (message.limit != null && message.hasOwnProperty('limit'))
          if (!$util.isInteger(message.limit)) return 'limit: integer expected';
        if (message.totalPage != null && message.hasOwnProperty('totalPage'))
          if (!$util.isInteger(message.totalPage))
            return 'totalPage: integer expected';
        if (message.count != null && message.hasOwnProperty('count'))
          if (!$util.isInteger(message.count)) return 'count: integer expected';
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan'))
          if (!$util.isString(message.lowerThan))
            return 'lowerThan: string expected';
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        )
          if (!$util.isString(message.greaterThan))
            return 'greaterThan: string expected';
        return null;
      };

      /**
       * Creates a Meta message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.ResponseGetAll.Meta} Meta
       */
      Meta.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.ResponseGetAll.Meta) return object;
        var message = new $root.Product.ResponseGetAll.Meta();
        if (object.pagination != null)
          message.pagination = object.pagination | 0;
        if (object.limit != null) message.limit = object.limit | 0;
        if (object.totalPage != null) message.totalPage = object.totalPage | 0;
        if (object.count != null) message.count = object.count | 0;
        if (object.lowerThan != null)
          message.lowerThan = String(object.lowerThan);
        if (object.greaterThan != null)
          message.greaterThan = String(object.greaterThan);
        return message;
      };

      /**
       * Creates a plain object from a Meta message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.ResponseGetAll.Meta
       * @static
       * @param {Product.ResponseGetAll.Meta} message Meta
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Meta.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.pagination = 0;
          object.limit = 0;
          object.totalPage = 0;
          object.count = 0;
          object.lowerThan = '';
          object.greaterThan = '';
        }
        if (message.pagination != null && message.hasOwnProperty('pagination'))
          object.pagination = message.pagination;
        if (message.limit != null && message.hasOwnProperty('limit'))
          object.limit = message.limit;
        if (message.totalPage != null && message.hasOwnProperty('totalPage'))
          object.totalPage = message.totalPage;
        if (message.count != null && message.hasOwnProperty('count'))
          object.count = message.count;
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan'))
          object.lowerThan = message.lowerThan;
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        )
          object.greaterThan = message.greaterThan;
        return object;
      };

      /**
       * Converts this Meta to JSON.
       * @function toJSON
       * @memberof Product.ResponseGetAll.Meta
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Meta.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Meta;
    })();

    return ResponseGetAll;
  })();

  Product.ResponseDetail = (function () {
    /**
     * Properties of a ResponseDetail.
     * @memberof Product
     * @interface IResponseDetail
     * @property {Product.ResponseGetAll.IData|null} [data] ResponseDetail data
     * @property {boolean|null} [success] ResponseDetail success
     * @property {string|null} [message] ResponseDetail message
     */

    /**
     * Constructs a new ResponseDetail.
     * @memberof Product
     * @classdesc Represents a ResponseDetail.
     * @implements IResponseDetail
     * @constructor
     * @param {Product.IResponseDetail=} [properties] Properties to set
     */
    function ResponseDetail(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseDetail data.
     * @member {Product.ResponseGetAll.IData|null|undefined} data
     * @memberof Product.ResponseDetail
     * @instance
     */
    ResponseDetail.prototype.data = null;

    /**
     * ResponseDetail success.
     * @member {boolean} success
     * @memberof Product.ResponseDetail
     * @instance
     */
    ResponseDetail.prototype.success = false;

    /**
     * ResponseDetail message.
     * @member {string} message
     * @memberof Product.ResponseDetail
     * @instance
     */
    ResponseDetail.prototype.message = '';

    /**
     * Creates a new ResponseDetail instance using the specified properties.
     * @function create
     * @memberof Product.ResponseDetail
     * @static
     * @param {Product.IResponseDetail=} [properties] Properties to set
     * @returns {Product.ResponseDetail} ResponseDetail instance
     */
    ResponseDetail.create = function create(properties) {
      return new ResponseDetail(properties);
    };

    /**
     * Encodes the specified ResponseDetail message. Does not implicitly {@link Product.ResponseDetail.verify|verify} messages.
     * @function encode
     * @memberof Product.ResponseDetail
     * @static
     * @param {Product.IResponseDetail} message ResponseDetail message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDetail.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Product.ResponseGetAll.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 2, wireType 0 =*/ 16).bool(message.success);
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified ResponseDetail message, length delimited. Does not implicitly {@link Product.ResponseDetail.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Product.ResponseDetail
     * @static
     * @param {Product.IResponseDetail} message ResponseDetail message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDetail.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer.
     * @function decode
     * @memberof Product.ResponseDetail
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Product.ResponseDetail} ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDetail.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Product.ResponseDetail();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.Product.ResponseGetAll.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 2:
            message.success = reader.bool();
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Product.ResponseDetail
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Product.ResponseDetail} ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDetail.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseDetail message.
     * @function verify
     * @memberof Product.ResponseDetail
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseDetail.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Product.ResponseGetAll.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseDetail message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Product.ResponseDetail
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Product.ResponseDetail} ResponseDetail
     */
    ResponseDetail.fromObject = function fromObject(object) {
      if (object instanceof $root.Product.ResponseDetail) return object;
      var message = new $root.Product.ResponseDetail();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Product.ResponseDetail.data: object expected');
        message.data = $root.Product.ResponseGetAll.Data.fromObject(
          object.data,
        );
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseDetail message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Product.ResponseDetail
     * @static
     * @param {Product.ResponseDetail} message ResponseDetail
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseDetail.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.success = false;
        object.message = '';
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Product.ResponseGetAll.Data.toObject(
          message.data,
          options,
        );
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      return object;
    };

    /**
     * Converts this ResponseDetail to JSON.
     * @function toJSON
     * @memberof Product.ResponseDetail
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseDetail.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ResponseDetail;
  })();

  Product.ResponseCreate = (function () {
    /**
     * Properties of a ResponseCreate.
     * @memberof Product
     * @interface IResponseCreate
     * @property {Product.ResponseGetAll.IData|null} [data] ResponseCreate data
     * @property {string|null} [message] ResponseCreate message
     * @property {boolean|null} [success] ResponseCreate success
     */

    /**
     * Constructs a new ResponseCreate.
     * @memberof Product
     * @classdesc Represents a ResponseCreate.
     * @implements IResponseCreate
     * @constructor
     * @param {Product.IResponseCreate=} [properties] Properties to set
     */
    function ResponseCreate(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseCreate data.
     * @member {Product.ResponseGetAll.IData|null|undefined} data
     * @memberof Product.ResponseCreate
     * @instance
     */
    ResponseCreate.prototype.data = null;

    /**
     * ResponseCreate message.
     * @member {string} message
     * @memberof Product.ResponseCreate
     * @instance
     */
    ResponseCreate.prototype.message = '';

    /**
     * ResponseCreate success.
     * @member {boolean} success
     * @memberof Product.ResponseCreate
     * @instance
     */
    ResponseCreate.prototype.success = false;

    /**
     * Creates a new ResponseCreate instance using the specified properties.
     * @function create
     * @memberof Product.ResponseCreate
     * @static
     * @param {Product.IResponseCreate=} [properties] Properties to set
     * @returns {Product.ResponseCreate} ResponseCreate instance
     */
    ResponseCreate.create = function create(properties) {
      return new ResponseCreate(properties);
    };

    /**
     * Encodes the specified ResponseCreate message. Does not implicitly {@link Product.ResponseCreate.verify|verify} messages.
     * @function encode
     * @memberof Product.ResponseCreate
     * @static
     * @param {Product.IResponseCreate} message ResponseCreate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseCreate.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Product.ResponseGetAll.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.message);
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 3, wireType 0 =*/ 24).bool(message.success);
      return writer;
    };

    /**
     * Encodes the specified ResponseCreate message, length delimited. Does not implicitly {@link Product.ResponseCreate.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Product.ResponseCreate
     * @static
     * @param {Product.IResponseCreate} message ResponseCreate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseCreate.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer.
     * @function decode
     * @memberof Product.ResponseCreate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Product.ResponseCreate} ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseCreate.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Product.ResponseCreate();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.Product.ResponseGetAll.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 2:
            message.message = reader.string();
            break;
          case 3:
            message.success = reader.bool();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Product.ResponseCreate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Product.ResponseCreate} ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseCreate.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseCreate message.
     * @function verify
     * @memberof Product.ResponseCreate
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseCreate.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Product.ResponseGetAll.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      return null;
    };

    /**
     * Creates a ResponseCreate message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Product.ResponseCreate
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Product.ResponseCreate} ResponseCreate
     */
    ResponseCreate.fromObject = function fromObject(object) {
      if (object instanceof $root.Product.ResponseCreate) return object;
      var message = new $root.Product.ResponseCreate();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Product.ResponseCreate.data: object expected');
        message.data = $root.Product.ResponseGetAll.Data.fromObject(
          object.data,
        );
      }
      if (object.message != null) message.message = String(object.message);
      if (object.success != null) message.success = Boolean(object.success);
      return message;
    };

    /**
     * Creates a plain object from a ResponseCreate message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Product.ResponseCreate
     * @static
     * @param {Product.ResponseCreate} message ResponseCreate
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseCreate.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.message = '';
        object.success = false;
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Product.ResponseGetAll.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      return object;
    };

    /**
     * Converts this ResponseCreate to JSON.
     * @function toJSON
     * @memberof Product.ResponseCreate
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseCreate.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ResponseCreate;
  })();

  Product.ResponseUpdate = (function () {
    /**
     * Properties of a ResponseUpdate.
     * @memberof Product
     * @interface IResponseUpdate
     * @property {Product.ResponseGetAll.IData|null} [data] ResponseUpdate data
     * @property {boolean|null} [success] ResponseUpdate success
     * @property {string|null} [message] ResponseUpdate message
     */

    /**
     * Constructs a new ResponseUpdate.
     * @memberof Product
     * @classdesc Represents a ResponseUpdate.
     * @implements IResponseUpdate
     * @constructor
     * @param {Product.IResponseUpdate=} [properties] Properties to set
     */
    function ResponseUpdate(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseUpdate data.
     * @member {Product.ResponseGetAll.IData|null|undefined} data
     * @memberof Product.ResponseUpdate
     * @instance
     */
    ResponseUpdate.prototype.data = null;

    /**
     * ResponseUpdate success.
     * @member {boolean} success
     * @memberof Product.ResponseUpdate
     * @instance
     */
    ResponseUpdate.prototype.success = false;

    /**
     * ResponseUpdate message.
     * @member {string} message
     * @memberof Product.ResponseUpdate
     * @instance
     */
    ResponseUpdate.prototype.message = '';

    /**
     * Creates a new ResponseUpdate instance using the specified properties.
     * @function create
     * @memberof Product.ResponseUpdate
     * @static
     * @param {Product.IResponseUpdate=} [properties] Properties to set
     * @returns {Product.ResponseUpdate} ResponseUpdate instance
     */
    ResponseUpdate.create = function create(properties) {
      return new ResponseUpdate(properties);
    };

    /**
     * Encodes the specified ResponseUpdate message. Does not implicitly {@link Product.ResponseUpdate.verify|verify} messages.
     * @function encode
     * @memberof Product.ResponseUpdate
     * @static
     * @param {Product.IResponseUpdate} message ResponseUpdate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseUpdate.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Product.ResponseGetAll.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.success);
      return writer;
    };

    /**
     * Encodes the specified ResponseUpdate message, length delimited. Does not implicitly {@link Product.ResponseUpdate.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Product.ResponseUpdate
     * @static
     * @param {Product.IResponseUpdate} message ResponseUpdate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseUpdate.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer.
     * @function decode
     * @memberof Product.ResponseUpdate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Product.ResponseUpdate} ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseUpdate.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Product.ResponseUpdate();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.Product.ResponseGetAll.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 4:
            message.success = reader.bool();
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Product.ResponseUpdate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Product.ResponseUpdate} ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseUpdate.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseUpdate message.
     * @function verify
     * @memberof Product.ResponseUpdate
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseUpdate.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Product.ResponseGetAll.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseUpdate message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Product.ResponseUpdate
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Product.ResponseUpdate} ResponseUpdate
     */
    ResponseUpdate.fromObject = function fromObject(object) {
      if (object instanceof $root.Product.ResponseUpdate) return object;
      var message = new $root.Product.ResponseUpdate();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Product.ResponseUpdate.data: object expected');
        message.data = $root.Product.ResponseGetAll.Data.fromObject(
          object.data,
        );
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseUpdate message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Product.ResponseUpdate
     * @static
     * @param {Product.ResponseUpdate} message ResponseUpdate
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseUpdate.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.message = '';
        object.success = false;
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Product.ResponseGetAll.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      return object;
    };

    /**
     * Converts this ResponseUpdate to JSON.
     * @function toJSON
     * @memberof Product.ResponseUpdate
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseUpdate.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ResponseUpdate;
  })();

  Product.ResponseDelete = (function () {
    /**
     * Properties of a ResponseDelete.
     * @memberof Product
     * @interface IResponseDelete
     * @property {Product.ResponseDelete.IData|null} [data] ResponseDelete data
     * @property {boolean|null} [success] ResponseDelete success
     * @property {string|null} [message] ResponseDelete message
     */

    /**
     * Constructs a new ResponseDelete.
     * @memberof Product
     * @classdesc Represents a ResponseDelete.
     * @implements IResponseDelete
     * @constructor
     * @param {Product.IResponseDelete=} [properties] Properties to set
     */
    function ResponseDelete(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseDelete data.
     * @member {Product.ResponseDelete.IData|null|undefined} data
     * @memberof Product.ResponseDelete
     * @instance
     */
    ResponseDelete.prototype.data = null;

    /**
     * ResponseDelete success.
     * @member {boolean} success
     * @memberof Product.ResponseDelete
     * @instance
     */
    ResponseDelete.prototype.success = false;

    /**
     * ResponseDelete message.
     * @member {string} message
     * @memberof Product.ResponseDelete
     * @instance
     */
    ResponseDelete.prototype.message = '';

    /**
     * Creates a new ResponseDelete instance using the specified properties.
     * @function create
     * @memberof Product.ResponseDelete
     * @static
     * @param {Product.IResponseDelete=} [properties] Properties to set
     * @returns {Product.ResponseDelete} ResponseDelete instance
     */
    ResponseDelete.create = function create(properties) {
      return new ResponseDelete(properties);
    };

    /**
     * Encodes the specified ResponseDelete message. Does not implicitly {@link Product.ResponseDelete.verify|verify} messages.
     * @function encode
     * @memberof Product.ResponseDelete
     * @static
     * @param {Product.IResponseDelete} message ResponseDelete message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDelete.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Product.ResponseDelete.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.message);
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.success);
      return writer;
    };

    /**
     * Encodes the specified ResponseDelete message, length delimited. Does not implicitly {@link Product.ResponseDelete.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Product.ResponseDelete
     * @static
     * @param {Product.IResponseDelete} message ResponseDelete message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDelete.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer.
     * @function decode
     * @memberof Product.ResponseDelete
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Product.ResponseDelete} ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDelete.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Product.ResponseDelete();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.Product.ResponseDelete.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 4:
            message.success = reader.bool();
            break;
          case 2:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Product.ResponseDelete
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Product.ResponseDelete} ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDelete.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseDelete message.
     * @function verify
     * @memberof Product.ResponseDelete
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseDelete.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Product.ResponseDelete.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseDelete message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Product.ResponseDelete
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Product.ResponseDelete} ResponseDelete
     */
    ResponseDelete.fromObject = function fromObject(object) {
      if (object instanceof $root.Product.ResponseDelete) return object;
      var message = new $root.Product.ResponseDelete();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Product.ResponseDelete.data: object expected');
        message.data = $root.Product.ResponseDelete.Data.fromObject(
          object.data,
        );
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseDelete message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Product.ResponseDelete
     * @static
     * @param {Product.ResponseDelete} message ResponseDelete
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseDelete.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.message = '';
        object.success = false;
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Product.ResponseDelete.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      return object;
    };

    /**
     * Converts this ResponseDelete to JSON.
     * @function toJSON
     * @memberof Product.ResponseDelete
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseDelete.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    ResponseDelete.Data = (function () {
      /**
       * Properties of a Data.
       * @memberof Product.ResponseDelete
       * @interface IData
       * @property {string|null} [message] Data message
       */

      /**
       * Constructs a new Data.
       * @memberof Product.ResponseDelete
       * @classdesc Represents a Data.
       * @implements IData
       * @constructor
       * @param {Product.ResponseDelete.IData=} [properties] Properties to set
       */
      function Data(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Data message.
       * @member {string} message
       * @memberof Product.ResponseDelete.Data
       * @instance
       */
      Data.prototype.message = '';

      /**
       * Creates a new Data instance using the specified properties.
       * @function create
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {Product.ResponseDelete.IData=} [properties] Properties to set
       * @returns {Product.ResponseDelete.Data} Data instance
       */
      Data.create = function create(properties) {
        return new Data(properties);
      };

      /**
       * Encodes the specified Data message. Does not implicitly {@link Product.ResponseDelete.Data.verify|verify} messages.
       * @function encode
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {Product.ResponseDelete.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.message != null &&
          Object.hasOwnProperty.call(message, 'message')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.message);
        return writer;
      };

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Product.ResponseDelete.Data.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {Product.ResponseDelete.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @function decode
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Product.ResponseDelete.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Product.ResponseDelete.Data();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.message = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Product.ResponseDelete.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Data message.
       * @function verify
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Data.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.message != null && message.hasOwnProperty('message'))
          if (!$util.isString(message.message))
            return 'message: string expected';
        return null;
      };

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Product.ResponseDelete.Data} Data
       */
      Data.fromObject = function fromObject(object) {
        if (object instanceof $root.Product.ResponseDelete.Data) return object;
        var message = new $root.Product.ResponseDelete.Data();
        if (object.message != null) message.message = String(object.message);
        return message;
      };

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Product.ResponseDelete.Data
       * @static
       * @param {Product.ResponseDelete.Data} message Data
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Data.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) object.message = '';
        if (message.message != null && message.hasOwnProperty('message'))
          object.message = message.message;
        return object;
      };

      /**
       * Converts this Data to JSON.
       * @function toJSON
       * @memberof Product.ResponseDelete.Data
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Data.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Data;
    })();

    return ResponseDelete;
  })();

  return Product;
})();

$root.google = (function () {
  /**
   * Namespace google.
   * @exports google
   * @namespace
   */
  var google = {};

  google.protobuf = (function () {
    /**
     * Namespace protobuf.
     * @memberof google
     * @namespace
     */
    var protobuf = {};

    protobuf.Any = (function () {
      /**
       * Properties of an Any.
       * @memberof google.protobuf
       * @interface IAny
       * @property {string|null} [type_url] Any type_url
       * @property {Uint8Array|null} [value] Any value
       */

      /**
       * Constructs a new Any.
       * @memberof google.protobuf
       * @classdesc Represents an Any.
       * @implements IAny
       * @constructor
       * @param {google.protobuf.IAny=} [properties] Properties to set
       */
      function Any(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Any type_url.
       * @member {string} type_url
       * @memberof google.protobuf.Any
       * @instance
       */
      Any.prototype.type_url = '';

      /**
       * Any value.
       * @member {Uint8Array} value
       * @memberof google.protobuf.Any
       * @instance
       */
      Any.prototype.value = $util.newBuffer([]);

      /**
       * Creates a new Any instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.IAny=} [properties] Properties to set
       * @returns {google.protobuf.Any} Any instance
       */
      Any.create = function create(properties) {
        return new Any(properties);
      };

      /**
       * Encodes the specified Any message. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.IAny} message Any message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Any.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.type_url != null &&
          Object.hasOwnProperty.call(message, 'type_url')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.type_url);
        if (
          message.value != null &&
          Object.hasOwnProperty.call(message, 'value')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).bytes(message.value);
        return writer;
      };

      /**
       * Encodes the specified Any message, length delimited. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.IAny} message Any message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Any.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes an Any message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Any
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Any} Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Any.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.Any();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.type_url = reader.string();
              break;
            case 2:
              message.value = reader.bytes();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes an Any message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Any
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Any} Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Any.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies an Any message.
       * @function verify
       * @memberof google.protobuf.Any
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Any.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.type_url != null && message.hasOwnProperty('type_url'))
          if (!$util.isString(message.type_url))
            return 'type_url: string expected';
        if (message.value != null && message.hasOwnProperty('value'))
          if (
            !(
              (message.value && typeof message.value.length === 'number') ||
              $util.isString(message.value)
            )
          )
            return 'value: buffer expected';
        return null;
      };

      /**
       * Creates an Any message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Any
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Any} Any
       */
      Any.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Any) return object;
        var message = new $root.google.protobuf.Any();
        if (object.type_url != null) message.type_url = String(object.type_url);
        if (object.value != null)
          if (typeof object.value === 'string')
            $util.base64.decode(
              object.value,
              (message.value = $util.newBuffer(
                $util.base64.length(object.value),
              )),
              0,
            );
          else if (object.value.length) message.value = object.value;
        return message;
      };

      /**
       * Creates a plain object from an Any message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.Any} message Any
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Any.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.type_url = '';
          if (options.bytes === String) object.value = '';
          else {
            object.value = [];
            if (options.bytes !== Array)
              object.value = $util.newBuffer(object.value);
          }
        }
        if (message.type_url != null && message.hasOwnProperty('type_url'))
          object.type_url = message.type_url;
        if (message.value != null && message.hasOwnProperty('value'))
          object.value =
            options.bytes === String
              ? $util.base64.encode(message.value, 0, message.value.length)
              : options.bytes === Array
              ? Array.prototype.slice.call(message.value)
              : message.value;
        return object;
      };

      /**
       * Converts this Any to JSON.
       * @function toJSON
       * @memberof google.protobuf.Any
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Any.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Any;
    })();

    protobuf.Struct = (function () {
      /**
       * Properties of a Struct.
       * @memberof google.protobuf
       * @interface IStruct
       * @property {Object.<string,google.protobuf.IValue>|null} [fields] Struct fields
       */

      /**
       * Constructs a new Struct.
       * @memberof google.protobuf
       * @classdesc Represents a Struct.
       * @implements IStruct
       * @constructor
       * @param {google.protobuf.IStruct=} [properties] Properties to set
       */
      function Struct(properties) {
        this.fields = {};
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Struct fields.
       * @member {Object.<string,google.protobuf.IValue>} fields
       * @memberof google.protobuf.Struct
       * @instance
       */
      Struct.prototype.fields = $util.emptyObject;

      /**
       * Creates a new Struct instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.IStruct=} [properties] Properties to set
       * @returns {google.protobuf.Struct} Struct instance
       */
      Struct.create = function create(properties) {
        return new Struct(properties);
      };

      /**
       * Encodes the specified Struct message. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.IStruct} message Struct message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Struct.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.fields != null &&
          Object.hasOwnProperty.call(message, 'fields')
        )
          for (
            var keys = Object.keys(message.fields), i = 0;
            i < keys.length;
            ++i
          ) {
            writer
              .uint32(/* id 1, wireType 2 =*/ 10)
              .fork()
              .uint32(/* id 1, wireType 2 =*/ 10)
              .string(keys[i]);
            $root.google.protobuf.Value.encode(
              message.fields[keys[i]],
              writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
            )
              .ldelim()
              .ldelim();
          }
        return writer;
      };

      /**
       * Encodes the specified Struct message, length delimited. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.IStruct} message Struct message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Struct.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Struct message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Struct
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Struct} Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Struct.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.Struct(),
          key,
          value;
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              if (message.fields === $util.emptyObject) message.fields = {};
              var end2 = reader.uint32() + reader.pos;
              key = '';
              value = null;
              while (reader.pos < end2) {
                var tag2 = reader.uint32();
                switch (tag2 >>> 3) {
                  case 1:
                    key = reader.string();
                    break;
                  case 2:
                    value = $root.google.protobuf.Value.decode(
                      reader,
                      reader.uint32(),
                    );
                    break;
                  default:
                    reader.skipType(tag2 & 7);
                    break;
                }
              }
              message.fields[key] = value;
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Struct message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Struct
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Struct} Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Struct.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Struct message.
       * @function verify
       * @memberof google.protobuf.Struct
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Struct.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.fields != null && message.hasOwnProperty('fields')) {
          if (!$util.isObject(message.fields)) return 'fields: object expected';
          var key = Object.keys(message.fields);
          for (var i = 0; i < key.length; ++i) {
            var error = $root.google.protobuf.Value.verify(
              message.fields[key[i]],
            );
            if (error) return 'fields.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a Struct message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Struct
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Struct} Struct
       */
      Struct.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Struct) return object;
        var message = new $root.google.protobuf.Struct();
        if (object.fields) {
          if (typeof object.fields !== 'object')
            throw TypeError('.google.protobuf.Struct.fields: object expected');
          message.fields = {};
          for (
            var keys = Object.keys(object.fields), i = 0;
            i < keys.length;
            ++i
          ) {
            if (typeof object.fields[keys[i]] !== 'object')
              throw TypeError(
                '.google.protobuf.Struct.fields: object expected',
              );
            message.fields[keys[i]] = $root.google.protobuf.Value.fromObject(
              object.fields[keys[i]],
            );
          }
        }
        return message;
      };

      /**
       * Creates a plain object from a Struct message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.Struct} message Struct
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Struct.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.objects || options.defaults) object.fields = {};
        var keys2;
        if (message.fields && (keys2 = Object.keys(message.fields)).length) {
          object.fields = {};
          for (var j = 0; j < keys2.length; ++j)
            object.fields[keys2[j]] = $root.google.protobuf.Value.toObject(
              message.fields[keys2[j]],
              options,
            );
        }
        return object;
      };

      /**
       * Converts this Struct to JSON.
       * @function toJSON
       * @memberof google.protobuf.Struct
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Struct.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Struct;
    })();

    protobuf.Value = (function () {
      /**
       * Properties of a Value.
       * @memberof google.protobuf
       * @interface IValue
       * @property {google.protobuf.NullValue|null} [nullValue] Value nullValue
       * @property {number|null} [numberValue] Value numberValue
       * @property {string|null} [stringValue] Value stringValue
       * @property {boolean|null} [boolValue] Value boolValue
       * @property {google.protobuf.IStruct|null} [structValue] Value structValue
       * @property {google.protobuf.IListValue|null} [listValue] Value listValue
       */

      /**
       * Constructs a new Value.
       * @memberof google.protobuf
       * @classdesc Represents a Value.
       * @implements IValue
       * @constructor
       * @param {google.protobuf.IValue=} [properties] Properties to set
       */
      function Value(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Value nullValue.
       * @member {google.protobuf.NullValue|null|undefined} nullValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.nullValue = null;

      /**
       * Value numberValue.
       * @member {number|null|undefined} numberValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.numberValue = null;

      /**
       * Value stringValue.
       * @member {string|null|undefined} stringValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.stringValue = null;

      /**
       * Value boolValue.
       * @member {boolean|null|undefined} boolValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.boolValue = null;

      /**
       * Value structValue.
       * @member {google.protobuf.IStruct|null|undefined} structValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.structValue = null;

      /**
       * Value listValue.
       * @member {google.protobuf.IListValue|null|undefined} listValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.listValue = null;

      // OneOf field names bound to virtual getters and setters
      var $oneOfFields;

      /**
       * Value kind.
       * @member {"nullValue"|"numberValue"|"stringValue"|"boolValue"|"structValue"|"listValue"|undefined} kind
       * @memberof google.protobuf.Value
       * @instance
       */
      Object.defineProperty(Value.prototype, 'kind', {
        get: $util.oneOfGetter(
          ($oneOfFields = [
            'nullValue',
            'numberValue',
            'stringValue',
            'boolValue',
            'structValue',
            'listValue',
          ]),
        ),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Creates a new Value instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.IValue=} [properties] Properties to set
       * @returns {google.protobuf.Value} Value instance
       */
      Value.create = function create(properties) {
        return new Value(properties);
      };

      /**
       * Encodes the specified Value message. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.IValue} message Value message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Value.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.nullValue != null &&
          Object.hasOwnProperty.call(message, 'nullValue')
        )
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.nullValue);
        if (
          message.numberValue != null &&
          Object.hasOwnProperty.call(message, 'numberValue')
        )
          writer.uint32(/* id 2, wireType 1 =*/ 17).double(message.numberValue);
        if (
          message.stringValue != null &&
          Object.hasOwnProperty.call(message, 'stringValue')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.stringValue);
        if (
          message.boolValue != null &&
          Object.hasOwnProperty.call(message, 'boolValue')
        )
          writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.boolValue);
        if (
          message.structValue != null &&
          Object.hasOwnProperty.call(message, 'structValue')
        )
          $root.google.protobuf.Struct.encode(
            message.structValue,
            writer.uint32(/* id 5, wireType 2 =*/ 42).fork(),
          ).ldelim();
        if (
          message.listValue != null &&
          Object.hasOwnProperty.call(message, 'listValue')
        )
          $root.google.protobuf.ListValue.encode(
            message.listValue,
            writer.uint32(/* id 6, wireType 2 =*/ 50).fork(),
          ).ldelim();
        return writer;
      };

      /**
       * Encodes the specified Value message, length delimited. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.IValue} message Value message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Value.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Value message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Value
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Value} Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Value.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.Value();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.nullValue = reader.int32();
              break;
            case 2:
              message.numberValue = reader.double();
              break;
            case 3:
              message.stringValue = reader.string();
              break;
            case 4:
              message.boolValue = reader.bool();
              break;
            case 5:
              message.structValue = $root.google.protobuf.Struct.decode(
                reader,
                reader.uint32(),
              );
              break;
            case 6:
              message.listValue = $root.google.protobuf.ListValue.decode(
                reader,
                reader.uint32(),
              );
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Value message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Value
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Value} Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Value.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Value message.
       * @function verify
       * @memberof google.protobuf.Value
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Value.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        var properties = {};
        if (message.nullValue != null && message.hasOwnProperty('nullValue')) {
          properties.kind = 1;
          switch (message.nullValue) {
            default:
              return 'nullValue: enum value expected';
            case 0:
              break;
          }
        }
        if (
          message.numberValue != null &&
          message.hasOwnProperty('numberValue')
        ) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          if (typeof message.numberValue !== 'number')
            return 'numberValue: number expected';
        }
        if (
          message.stringValue != null &&
          message.hasOwnProperty('stringValue')
        ) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          if (!$util.isString(message.stringValue))
            return 'stringValue: string expected';
        }
        if (message.boolValue != null && message.hasOwnProperty('boolValue')) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          if (typeof message.boolValue !== 'boolean')
            return 'boolValue: boolean expected';
        }
        if (
          message.structValue != null &&
          message.hasOwnProperty('structValue')
        ) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          {
            var error = $root.google.protobuf.Struct.verify(
              message.structValue,
            );
            if (error) return 'structValue.' + error;
          }
        }
        if (message.listValue != null && message.hasOwnProperty('listValue')) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          {
            var error = $root.google.protobuf.ListValue.verify(
              message.listValue,
            );
            if (error) return 'listValue.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a Value message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Value
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Value} Value
       */
      Value.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Value) return object;
        var message = new $root.google.protobuf.Value();
        switch (object.nullValue) {
          case 'NULL_VALUE':
          case 0:
            message.nullValue = 0;
            break;
        }
        if (object.numberValue != null)
          message.numberValue = Number(object.numberValue);
        if (object.stringValue != null)
          message.stringValue = String(object.stringValue);
        if (object.boolValue != null)
          message.boolValue = Boolean(object.boolValue);
        if (object.structValue != null) {
          if (typeof object.structValue !== 'object')
            throw TypeError(
              '.google.protobuf.Value.structValue: object expected',
            );
          message.structValue = $root.google.protobuf.Struct.fromObject(
            object.structValue,
          );
        }
        if (object.listValue != null) {
          if (typeof object.listValue !== 'object')
            throw TypeError(
              '.google.protobuf.Value.listValue: object expected',
            );
          message.listValue = $root.google.protobuf.ListValue.fromObject(
            object.listValue,
          );
        }
        return message;
      };

      /**
       * Creates a plain object from a Value message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.Value} message Value
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Value.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (message.nullValue != null && message.hasOwnProperty('nullValue')) {
          object.nullValue =
            options.enums === String
              ? $root.google.protobuf.NullValue[message.nullValue]
              : message.nullValue;
          if (options.oneofs) object.kind = 'nullValue';
        }
        if (
          message.numberValue != null &&
          message.hasOwnProperty('numberValue')
        ) {
          object.numberValue =
            options.json && !isFinite(message.numberValue)
              ? String(message.numberValue)
              : message.numberValue;
          if (options.oneofs) object.kind = 'numberValue';
        }
        if (
          message.stringValue != null &&
          message.hasOwnProperty('stringValue')
        ) {
          object.stringValue = message.stringValue;
          if (options.oneofs) object.kind = 'stringValue';
        }
        if (message.boolValue != null && message.hasOwnProperty('boolValue')) {
          object.boolValue = message.boolValue;
          if (options.oneofs) object.kind = 'boolValue';
        }
        if (
          message.structValue != null &&
          message.hasOwnProperty('structValue')
        ) {
          object.structValue = $root.google.protobuf.Struct.toObject(
            message.structValue,
            options,
          );
          if (options.oneofs) object.kind = 'structValue';
        }
        if (message.listValue != null && message.hasOwnProperty('listValue')) {
          object.listValue = $root.google.protobuf.ListValue.toObject(
            message.listValue,
            options,
          );
          if (options.oneofs) object.kind = 'listValue';
        }
        return object;
      };

      /**
       * Converts this Value to JSON.
       * @function toJSON
       * @memberof google.protobuf.Value
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Value.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Value;
    })();

    /**
     * NullValue enum.
     * @name google.protobuf.NullValue
     * @enum {number}
     * @property {number} NULL_VALUE=0 NULL_VALUE value
     */
    protobuf.NullValue = (function () {
      var valuesById = {},
        values = Object.create(valuesById);
      values[(valuesById[0] = 'NULL_VALUE')] = 0;
      return values;
    })();

    protobuf.ListValue = (function () {
      /**
       * Properties of a ListValue.
       * @memberof google.protobuf
       * @interface IListValue
       * @property {Array.<google.protobuf.IValue>|null} [values] ListValue values
       */

      /**
       * Constructs a new ListValue.
       * @memberof google.protobuf
       * @classdesc Represents a ListValue.
       * @implements IListValue
       * @constructor
       * @param {google.protobuf.IListValue=} [properties] Properties to set
       */
      function ListValue(properties) {
        this.values = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * ListValue values.
       * @member {Array.<google.protobuf.IValue>} values
       * @memberof google.protobuf.ListValue
       * @instance
       */
      ListValue.prototype.values = $util.emptyArray;

      /**
       * Creates a new ListValue instance using the specified properties.
       * @function create
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.IListValue=} [properties] Properties to set
       * @returns {google.protobuf.ListValue} ListValue instance
       */
      ListValue.create = function create(properties) {
        return new ListValue(properties);
      };

      /**
       * Encodes the specified ListValue message. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.IListValue} message ListValue message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      ListValue.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.values != null && message.values.length)
          for (var i = 0; i < message.values.length; ++i)
            $root.google.protobuf.Value.encode(
              message.values[i],
              writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
            ).ldelim();
        return writer;
      };

      /**
       * Encodes the specified ListValue message, length delimited. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.IListValue} message ListValue message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      ListValue.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a ListValue message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.ListValue
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.ListValue} ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      ListValue.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.ListValue();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              if (!(message.values && message.values.length))
                message.values = [];
              message.values.push(
                $root.google.protobuf.Value.decode(reader, reader.uint32()),
              );
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a ListValue message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.ListValue
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.ListValue} ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      ListValue.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a ListValue message.
       * @function verify
       * @memberof google.protobuf.ListValue
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      ListValue.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.values != null && message.hasOwnProperty('values')) {
          if (!Array.isArray(message.values)) return 'values: array expected';
          for (var i = 0; i < message.values.length; ++i) {
            var error = $root.google.protobuf.Value.verify(message.values[i]);
            if (error) return 'values.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a ListValue message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.ListValue
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.ListValue} ListValue
       */
      ListValue.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.ListValue) return object;
        var message = new $root.google.protobuf.ListValue();
        if (object.values) {
          if (!Array.isArray(object.values))
            throw TypeError(
              '.google.protobuf.ListValue.values: array expected',
            );
          message.values = [];
          for (var i = 0; i < object.values.length; ++i) {
            if (typeof object.values[i] !== 'object')
              throw TypeError(
                '.google.protobuf.ListValue.values: object expected',
              );
            message.values[i] = $root.google.protobuf.Value.fromObject(
              object.values[i],
            );
          }
        }
        return message;
      };

      /**
       * Creates a plain object from a ListValue message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.ListValue} message ListValue
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      ListValue.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.values = [];
        if (message.values && message.values.length) {
          object.values = [];
          for (var j = 0; j < message.values.length; ++j)
            object.values[j] = $root.google.protobuf.Value.toObject(
              message.values[j],
              options,
            );
        }
        return object;
      };

      /**
       * Converts this ListValue to JSON.
       * @function toJSON
       * @memberof google.protobuf.ListValue
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      ListValue.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return ListValue;
    })();

    return protobuf;
  })();

  return google;
})();

module.exports = $root;
