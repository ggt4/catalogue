import * as dotenv from 'dotenv';
dotenv.config();
export const serviceName: string = process.env.SERVICE_NAME;
export const serviceCode: string = process.env.SERVICE_CODE;
export const basePath: string = process.env.BASE_PATH;
export const mongoDbUrl: string = process.env.MONGODB_URL;
export const port: number = +process.env.PORT || 3000;

