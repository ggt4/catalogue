import { ProducerModule } from './producers/producer.module';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { mongoDbUrl } from './configs/env.config';

@Module({
  imports: [MongooseModule.forRoot(mongoDbUrl), ProducerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply().exclude().forRoutes();
  }
}
