import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { seeder } from 'nestjs-seeder';
import databaseConfig from './configs/database.config';
import { Product } from './producers/product/constant/product.constant';
import { ProductSchema } from './producers/product/schema/product.schema';
import { ProductSeeder } from './seeders/product.seeder';


seeder({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],
    }),
    MongooseModule.forRoot(process.env.MONGODB_URL),
    MongooseModule.forFeature([
      {
        name: Product,
        schema: ProductSchema,
      },
    ]),
  ],
}).run([ProductSeeder]);
