import { Metadata } from "grpc";

export interface MetaData extends Metadata {
    token: string;
}