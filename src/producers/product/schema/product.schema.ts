import * as mongoose from 'mongoose';
const schema = mongoose.Schema;

export const ProductSchema = new schema(
  {
    "productName": { type: String, default: '' },
    "productLogo": { type: String, default: '' },
    "productDescription": { type: String, default: '' },
    "productShortDescription": { type: String, default: '' },
    "metaTitleProduct": { type: String, default: '' },
    "metaDescriptionProduct": { type: String, default: '' },
    "productSlug": { type: String, default: '' },
    "metaKeyword": { type: String, default: '' },
    "price": { type: Number, default: 0 },
    "productImage": [{ type: schema.Types.Mixed, default: null }],
    "productCategory": [{ type: schema.Types.Mixed, default: null }],
    createdBy: { type: Number, default: 0 },
    createdAt: { type: Date, default: new Date() },
    updatedAt: { type: Date, default: new Date() },
    deletedAt: { type: Date, default: null },
  },
  {
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
    toJSON: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  },
);
