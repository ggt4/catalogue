import { BaseInterface } from 'src/utils/base/base.interface';

export interface ProductImage {
  nameImage: string;
  altImage: string;
  createdDate: any;
  updatedDate: any;
}

export interface ProductCategory {
  categoryName: string;
  categorySlug: string;
}

export interface ProductInterface extends BaseInterface {
  productName: string;
  productLogo: string;
  productDescription: string;
  productShortDescription: string;
  metaTitleProduct: string;
  metaDescriptionProduct: string;
  productSlug: string;
  metaKeyword: string;
  price: number;
  productImage: ProductImage[];
  productCategory: ProductCategory[];
}