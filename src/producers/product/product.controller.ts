import { FilterQuery, Types } from 'mongoose';
import { createPaginationOptions, Pagination } from 'src/utils/pagination';
import { ProductInterface } from './interface/product.interface';
import { ProductService } from './product.service';
import { Message } from 'src/utils/constants/response-message';
import { GrpcMethod } from '@nestjs/microservices';
import { Product } from 'proto-bundle/protoBundle';
import { Controller } from '@nestjs/common';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @GrpcMethod('Greeter', 'products')
  async findAll(req: Product.IRequest): Promise<Product.IResponseGetAll> {
    try {
      const { filterBy, search, ids } = req.query || {};

      const paginationOptions = createPaginationOptions(req);

      const filter: FilterQuery<ProductInterface> = {
        deletedAt: null,
      };

      if (
        filterBy &&
        search
      ) {
        filter[filterBy] = {
          $regex: `.*${search}.*`,
          $options: 'i',
        };
      }

      if (typeof ids && ids instanceof Array && ids.length) {
        const objectIds = ids.map((id) => Types.ObjectId(id));
        filter._id = { $in: objectIds };
      }

      const data = await this.productService.findAll(filter, paginationOptions);
      const pagination = new Pagination(null, paginationOptions);

      const result: Product.IResponseGetAll = {
        data,
        message: 'Successfully get submissions data.',
        success: true,
        meta: pagination,
      };

      return result;
    } catch (e) {
      const result: Product.IResponseGetAll = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'productCreate')
  async create(req: Product.IRequest): Promise<Product.IResponseCreate> {
    try {
      const { body } = req;
      body.createdBy = req.user.id || 0;
      const data = await this.productService.create(body);

      const result: Product.IResponseCreate = {
        data: data as Product.ResponseGetAll.IData,
        message: Message.CREATED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: Product.IResponseCreate = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'product')
  async submission(req: Product.IRequest): Promise<Product.IResponseDetail> {
    try {
      const { params } = req;
      const filter: FilterQuery<any> = {
        deletedAt: { $eq: null },
        _id: { $eq: Types.ObjectId(params.id) },
      };
      
      const data = await this.productService.findOne(filter);

      const result: Product.IResponseDetail = {
        data: data as Product.ResponseGetAll.IData,
        message: Message.CREATED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: Product.IResponseDetail = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'productUpdate')
  async update(req: Product.IRequest): Promise<Product.IResponseUpdate> {
    try {
      const { params, body } = req;
      const data = await this.productService.update(params.id, body);

      const result: Product.IResponseUpdate = {
        data: data as Product.ResponseGetAll.IData,
        message: Message.CREATED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: Product.IResponseUpdate = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'productDelete')
  async delete(req: Product.IRequest): Promise<Product.IResponseDelete> {
    try {
      const { params, body } = req;
      const data = await this.productService.remove(params.id);

      const result: Product.IResponseDelete = {
        data: null,
        message: Message.DELETED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: Product.IResponseDelete = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }
}
