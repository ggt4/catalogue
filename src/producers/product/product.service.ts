import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { PaginationOptions } from 'src/utils/pagination';
import { Product } from './constant/product.constant';
import { ProductInterface } from './interface/product.interface';

@Injectable()
export class ProductService {
  constructor(@InjectModel(Product) private productModel: Model<ProductInterface>) {}

  async findAll(
    filter: FilterQuery<ProductInterface>,
    paginationOptions: PaginationOptions,
  ) {
    const { skip, limit } = paginationOptions;
    const pipelines: any = [
      {
        $match: filter,
      },
      {
        $addFields: {
          id: '$_id',
        },
      },
      { $unset: ['_id', '__v'] },
    ];

    if (limit !== null && skip !== null) {
      pipelines.push({ $skip: skip }, { $limit: limit });
    }

    return await this.productModel.aggregate(pipelines).exec();
  }

  async findOne(filter) {
    const pipelines: any = [
      {
        $match: filter,
      },
      {
        $addFields: {
          id: '$_id',
        },
      },
      { $unset: ['_id', '__v'] },
    ];

    return await this.productModel
      .aggregate(pipelines)
      .exec()
      .then((items) => {
        if (items instanceof Array && items.length) {
          return items[0];
        }
        throw 'not found or not exist';
      });
  }

  async create(productDTO) {
    productDTO.createdAt = new Date();
    productDTO.updatedAt = new Date();

    const createProduct = new this.productModel(productDTO);
    await createProduct.save();
    return createProduct;
  }

  async update(id: string, productDTO) {
    productDTO.updatedAt = new Date();
    await this.productModel.findByIdAndUpdate(id, productDTO);
    return await this.productModel.findById(id);
  }

  async remove(id: string) {
    const productDTO = {
      updatedAt: new Date(),
      deletedAt: new Date(),
    }
    await this.productModel.findByIdAndUpdate(id, productDTO);
    return await this.productModel.findById(id);
  }
}
