import { NestFactory } from '@nestjs/core';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { AppModule } from './app.module';
import { basePath, port } from './configs/env.config';
import { grpcServerOptions } from './configs/grpc.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: true,
  });
  app.enableCors();
  app.setGlobalPrefix(process.env.BASE_PATH);
  const config = new DocumentBuilder()
    .setTitle('Form Builder API Docs')
    .setVersion('1.0')
    .setBasePath(process.env.BASE_PATH)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
  };
  SwaggerModule.setup(basePath + '/explorer', app, document, customOptions);

  app.connectMicroservice(grpcServerOptions);
  await app.startAllMicroservicesAsync();
  await app.listen(port);
}
(async () => await bootstrap())();
