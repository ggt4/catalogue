export const datas: any[] = [        {
  "id": 23,
  "productName": "DASTER ARIANA COUPLE IBU & ANAK -DASTER MURAH -DAS",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16150144186342021030614.jpeg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DASTER ARIANA COUPLE IBU &amp; ANAK</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Daster model terbaru dengan konsep couple dengan anak perempuan, harga tertera sudah satu set couple Ibu &amp; Anak. Menggunakan bahan rayon super adem dan nyaman banget dipakai.</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL DASTER MOMS :</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Lingkar Dada 110-120cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Panjang Daster 104cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Bahan Rayon Adem</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Bahan Kain Utuh dan Sudah diobras</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Ada Kantong dibagian Kanan</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Kancing Hidup &amp; Busui Friendly</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL DASTER ANAK:</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Lingkar Dada 80cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Panjang Daster Anak 52cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Bahan Rayon Adem</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Bahan Kain Utuh dan Sudah diobras</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Bisa dipadukan dengan legging anak</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Size 2-4 Tahun (usia hanya estimasi, tergantung besar anak)</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Note??</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??REAL PICT 95%, 5% karena efek pencahayaan</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Selama barang masih bisa di klik barang masih ready stok</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Untuk barang kurang atau reject mohon sertakan video unboxing</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">??Tidak menerima komplain dikarenakan tidak membaca deskripsi produk</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Happy Shoping Dear??</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "DASTER ARIANA COUPLE IBU & ANAK -DASTER MURAH -DASTER RAYON ADEM - PINK",
  "metaDescriptionProduct": "DASTER ARIANA COUPLE IBU & ANAK -DASTER MURAH -DASTER RAYON ADEM - PINK",
  "productSlug": "daster-ariana-couple-ibu--anak--daster-murah--daster-rayon-adem---pink",
  "metaKeyword": "DASTER ARIANA COUPLE IBU & ANAK -DASTER MURAH -DASTER RAYON ADEM - PINK",
  "price": 85000,
  "productImage": [
      {
          "id": 137,
          "productId": 23,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150085324532021030612.jpeg",
          "altImage": "pink",
          "createdDate": "2021-03-21T18:46:41.000Z",
          "updatedDate": "2021-03-21T18:46:41.000Z"
      },
      {
          "id": 138,
          "productId": 23,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150085542332021030612.jpeg",
          "altImage": "blue",
          "createdDate": "2021-03-21T18:46:41.000Z",
          "updatedDate": "2021-03-21T18:46:41.000Z"
      },
      {
          "id": 139,
          "productId": 23,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150085648042021030612.jpeg",
          "altImage": "purple",
          "createdDate": "2021-03-21T18:46:41.000Z",
          "updatedDate": "2021-03-21T18:46:41.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Daster",
          "categorySlug": "daster"
      }
  ]
},
{
  "id": 22,
  "productName": "LONG DRESS TALI MURAH || DRESS MURAH || DRESS RAYO",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16150061955712021030611.jpeg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DRESS TALI KEKINIAN</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Dress tali bisa dipadukan dengan cardigan atau manset untuk yang berhijab.</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Lingkar Dada 110-115 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Panjang Dress 120 CM</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Bahan Rayon Adem</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Sangat nyaman dipakai :)</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "LONG DRESS TALI MURAH || DRESS MURAH || DRESS RAYON ADEM",
  "metaDescriptionProduct": "LONG DRESS TALI MURAH || DRESS MURAH || DRESS RAYON ADEM",
  "productSlug": "long-dress-tali-murah--dress-murah--dress-rayon-adem",
  "metaKeyword": "LONG DRESS TALI MURAH || DRESS MURAH || DRESS RAYON ADEM",
  "price": 55000,
  "productImage": [
      {
          "id": 142,
          "productId": 22,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150062021492021030611.jpeg",
          "altImage": null,
          "createdDate": "2021-03-22T21:45:40.000Z",
          "updatedDate": "2021-03-22T21:45:40.000Z"
      },
      {
          "id": 143,
          "productId": 22,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150062111872021030611.jpeg",
          "altImage": null,
          "createdDate": "2021-03-22T21:45:40.000Z",
          "updatedDate": "2021-03-22T21:45:40.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Dress",
          "categorySlug": "dress"
      }
  ]
},
{
  "id": 21,
  "productName": "DASTER COUPLE IBU & ANAK || DASTER SET MURAH || DA",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16150056860232021030611.jpeg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DASTER TALI (KAIN UTUH)</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL DASTER MOM :</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">LD 115 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">PJ 100 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">JAHITAN SUDAH OBRAS</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BAHAN RAYON SANTUNG (ADEM BANGET DIPAKE)</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL DASTER ANAK</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">SIZE 1-2 THN BISA JADI DRESS</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">SIZE 3-4 BISA SEBAGAI ATASAN</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">CEK DETAIL SEBELUM MEMBELI :)</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "DASTER COUPLE IBU & ANAK || DASTER SET MURAH || DASTER RAYON ADEM",
  "metaDescriptionProduct": "DASTER COUPLE IBU & ANAK || DASTER SET MURAH || DASTER RAYON ADEM",
  "productSlug": "daster-couple-ibu--anak--daster-set-murah--daster-rayon-adem",
  "metaKeyword": "DASTER COUPLE IBU & ANAK || DASTER SET MURAH || DASTER RAYON ADEM",
  "price": 65000,
  "productImage": [
      {
          "id": 133,
          "productId": 21,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150056964132021030611.jpeg",
          "altImage": null,
          "createdDate": "2021-03-21T18:42:52.000Z",
          "updatedDate": "2021-03-21T18:42:52.000Z"
      },
      {
          "id": 134,
          "productId": 21,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150057081162021030611.jpeg",
          "altImage": null,
          "createdDate": "2021-03-21T18:42:52.000Z",
          "updatedDate": "2021-03-21T18:42:52.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Daster",
          "categorySlug": "daster"
      }
  ]
},
{
  "id": 20,
  "productName": "ONE SET ABSTRACT BLUE || PAJAMAS ADEM || BAHAN RAY",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16150052047572021030611.jpeg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL PRODUK</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Lingkar Dada 110 cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Panjang Baju 72 cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Panjang Lengan 68 cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Panjang Celana 90 cm</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Lebar Paha 66 cm</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Bahan Rayon Adem Banget.</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Happy Shopping Dear :*</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "https://www.tokopedia.com/desaristore/one-set-abstract-blue-murah-pajamas-adem-bahan-rayon",
  "metaDescriptionProduct": "https://www.tokopedia.com/desaristore/one-set-abstract-blue-murah-pajamas-adem-bahan-rayon",
  "productSlug": "one-set-abstract-blue--pajamas-adem--bahan-rayon",
  "metaKeyword": "https://www.tokopedia.com/desaristore/one-set-abstract-blue-murah-pajamas-adem-bahan-rayon",
  "price": 95000,
  "productImage": [
      {
          "id": 131,
          "productId": 20,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150052301712021030611.jpeg",
          "altImage": null,
          "createdDate": "2021-03-21T18:41:22.000Z",
          "updatedDate": "2021-03-21T18:41:22.000Z"
      },
      {
          "id": 132,
          "productId": 20,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16150052438762021030611.jpeg",
          "altImage": null,
          "createdDate": "2021-03-21T18:41:22.000Z",
          "updatedDate": "2021-03-21T18:41:22.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "One Set",
          "categorySlug": "one-set"
      }
  ]
},
{
  "id": 17,
  "productName": "DASTER IBU JUMBO - DASTER GEMES",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16140866745632021022320.jpg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DASTER JUMBO ALL SIZE</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">LD 120 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">PJ 115 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BUSUI FRIENDLY</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">KANCING 2 HIDUP</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">JAHITAN RAPI SUDAH DIOBRAS</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BAHAN WOLFIS PREMIUM ADEM BANGET</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">CEK DETAIL SEBELUM MEMBELI :)</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "DASTER IBU JUMBO - DASTER GEMES - DASTER MURAH",
  "metaDescriptionProduct": "DASTER IBU JUMBO - DASTER GEMES - DASTER MURAH",
  "productSlug": "DASTER-IBU-JUMBO-DASTER-GEMES",
  "metaKeyword": "DASTER IBU JUMBO - DASTER GEMES - DASTER MURAH",
  "price": 25000,
  "productImage": [
      {
          "id": 126,
          "productId": 17,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140867424502021022320.jpg",
          "altImage": "detail",
          "createdDate": "2021-03-21T18:38:17.000Z",
          "updatedDate": "2021-03-21T18:38:17.000Z"
      },
      {
          "id": 127,
          "productId": 17,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140867578702021022320.jpg",
          "altImage": "full",
          "createdDate": "2021-03-21T18:38:17.000Z",
          "updatedDate": "2021-03-21T18:38:17.000Z"
      },
      {
          "id": 128,
          "productId": 17,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140867719942021022320.jpg",
          "altImage": "left",
          "createdDate": "2021-03-21T18:38:17.000Z",
          "updatedDate": "2021-03-21T18:38:17.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Daster",
          "categorySlug": "daster"
      }
  ]
},
{
  "id": 16,
  "productName": "DASTER MANOHARA - DASTER GEMES",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16131430372592021021222.jpg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DASTER MANOHARA</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">STOK TERBATAS</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">SIAPA CEPAT DIA DAPAT ?</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BAHAN RAYON PREMIUM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">LD 100</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">PJ 105</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Happy Shopping Dear :)</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "DASTER MANOHARA - DASTER GEMES",
  "metaDescriptionProduct": "DASTER MANOHARA - DASTER GEMES",
  "productSlug": "daster-manohara-daster-gemes",
  "metaKeyword": "DASTER MANOHARA - DASTER GEMESc",
  "price": 38000,
  "productImage": [
      {
          "id": 119,
          "productId": 16,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140961248152021022323.jpg",
          "altImage": "full",
          "createdDate": "2021-03-21T18:32:25.000Z",
          "updatedDate": "2021-03-21T18:32:25.000Z"
      },
      {
          "id": 120,
          "productId": 16,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140961355702021022323.jpg",
          "altImage": "detail 1",
          "createdDate": "2021-03-21T18:32:25.000Z",
          "updatedDate": "2021-03-21T18:32:25.000Z"
      },
      {
          "id": 121,
          "productId": 16,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140961483192021022323.jpg",
          "altImage": "detail 2",
          "createdDate": "2021-03-21T18:32:25.000Z",
          "updatedDate": "2021-03-21T18:32:25.000Z"
      },
      {
          "id": 122,
          "productId": 16,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140961603142021022323.jpg",
          "altImage": "detail 3",
          "createdDate": "2021-03-21T18:32:25.000Z",
          "updatedDate": "2021-03-21T18:32:25.000Z"
      },
      {
          "id": 123,
          "productId": 16,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16140961692292021022323.jpg",
          "altImage": "detail 4",
          "createdDate": "2021-03-21T18:32:25.000Z",
          "updatedDate": "2021-03-21T18:32:25.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Daster",
          "categorySlug": "daster"
      }
  ]
},
{
  "id": 15,
  "productName": "ONE SET CELANA PENDEK - ONE SET ADEM",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16131362272432021021220.jpg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">SETELAN CELANA PENDEK</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">LD 106 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">PJ BJU 68 CM</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">PJ CELANA 49 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BAGIAN PINGGANG FULL KARET</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BAHAN SANTUNG</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Happy Shopping Dear :)</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "ONE SET CELANA PENDEK - ONE SET ADEM",
  "metaDescriptionProduct": "ONE SET CELANA PENDEK - ONE SET ADEM",
  "productSlug": "one-set-celana-pendek-one-set-adem",
  "metaKeyword": "ONE SET CELANA PENDEK - ONE SET ADEM",
  "price": 35000,
  "productImage": [
      {
          "id": 110,
          "productId": 15,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16131367880262021021220.jpg",
          "altImage": "full",
          "createdDate": "2021-03-20T10:13:18.000Z",
          "updatedDate": "2021-03-20T10:13:18.000Z"
      },
      {
          "id": 111,
          "productId": 15,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16131368487612021021220.jpg",
          "altImage": "detail",
          "createdDate": "2021-03-20T10:13:18.000Z",
          "updatedDate": "2021-03-20T10:13:18.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Daster",
          "categorySlug": "daster"
      }
  ]
},
{
  "id": 14,
  "productName": "DASTER COUPLE TALI ANAK TANGGUNG BATIK RAYON - DAS",
  "productLogo": "https://gateway.siafushop.com/api/v1/storage/product/16131330086022021021219.jpg",
  "productDescription": "<p><span style=\"color: rgba(49, 53, 59, 0.96);\">DASTER COUPLE TALI, ANAK TANGGUNG</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">BAHAN RAYON ADEM BANGET ?</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL DASTER MOM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">LD 100 CM</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">PJ 95 CM</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">DETAIL DASTER ANAK</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">SIZE 3-8 TAHUN</span></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">TERGANTUNG BESAR ANAK</span></p><p><br></p><p><span style=\"color: rgba(49, 53, 59, 0.96);\">Happy Shopping Dear</span></p>",
  "productShortDescription": "",
  "metaTitleProduct": "DASTER COUPLE TALI ANAK TANGGUNG BATIK RAYON - DASTER MURAH",
  "metaDescriptionProduct": "DASTER COUPLE TALI ANAK TANGGUNG BATIK RAYON - DASTER MURAH",
  "productSlug": "daster-couple-tali-anak-tanggung-batik-rayon",
  "metaKeyword": "DASTER COUPLE TALI ANAK TANGGUNG BATIK RAYON - DASTER MURAH",
  "price": 75000,
  "productImage": [
      {
          "id": 112,
          "productId": 14,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16131329093332021021219.jpg",
          "altImage": "full",
          "createdDate": "2021-03-21T18:29:47.000Z",
          "updatedDate": "2021-03-21T18:29:47.000Z"
      },
      {
          "id": 113,
          "productId": 14,
          "nameImage": "https://gateway.siafushop.com/api/v1/storage/product/16131329666022021021219.jpg",
          "altImage": "detail",
          "createdDate": "2021-03-21T18:29:47.000Z",
          "updatedDate": "2021-03-21T18:29:47.000Z"
      }
  ],
  "productCategory": [
      {
          "categoryName": "Daster",
          "categorySlug": "daster"
      }
  ]
}]