import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Seeder } from 'nestjs-seeder';
import { ProductInterface } from 'src/producers/product/interface/product.interface';
import { datas } from './data';
@Injectable()
export class ProductSeeder implements Seeder {
  constructor(
    @InjectModel('Product') private productModel: Model<ProductInterface>,
  ) {}

  async seed(): Promise<any> {
    const fts = await this.productModel.find().exec();
    if (fts.length <= 0) {
      return await this.productModel.insertMany(datas);
    }
    return null;
  }

  async drop(): Promise<any> {
    return this.productModel.deleteMany();
  }
}
